# Python code to find redshifts #

This is a repository for simple python code to find redshifts. At the moment only a conversion of Ivan Baldry's [AutoZ IDL code](http://www.astro.ljmu.ac.uk/~ikb/research/autoz_code/) is available, please reference that code and accompanying paper when you use this code. 


### Installation instructions ###

Put this somewhere in your `PYTHONPATH` to be able to load this.

For the emission line fitting to work well, you must have installed
either [mpfit](https://code.google.com/archive/p/astrolibpy/) or
[lmfit](https://lmfit.github.io/lmfit-py/). I recommend `lmfit`
because for `mpfit` it is necessary to make some small modifications
to the code for it to work. 

### Basic usage ###

To use the code you first need to define an AutoZ object. To do this
properly you need to provide a file with cross-correlation templates. 

    # Create an AutoZ object
    template_file = 'all_temps_v13-11-15-temp33-at-z.fits'
    fz = az.AutoZ(templatefile=template_file)

The next step is to set some processing parameters. There are some
useful defaults set here but it is best to at least set the wavelength
range of the spectra explicitly:

    # Set processing parameters
    fz.setup_processing(start=4750., end=9300.)

Then you need to create the wavelength array used for
cross-correlation, default values are normally ok. Then rebin the
template library onto this wavelength array. In this example I also
select a few templates.

    # Create the output wavelength array.
    fz.create_wavelength_array(verbose=True)
    
    # And create the new template library.
    to_use = np.concatenate((np.arange(2, 15), np.array([28, 29, 33])))
    fz.rebin_templates(templates_to_use=to_use)

After you have done this, you are ready to determine
redshifts. Assuming you have the flux and its uncertainty in variables
`f` and `df`, and the wavelength in variable `l`, you can do:

	r = fz.run_single_spec(f.copy(), df.copy(), l.copy(), in_vacuum=False)

This returns a `AutoZResult` object. To see the redshifts you just
print `r.z_peak`. By default the four highest peaks are kept and
stored there, sorted by significance. You can also request the
probability of these being right, using the GAMA calibration in Baldry
et al: 

	FoM, prob = r.calculate_FoM_GAMA()

Where `prob` is the probability that the redshift is correct. This is
not recalibrated for MUSE data so take this with a big grain of
salt. The redshift uncertainty is less problematic:

	z_unc_vel = r.calculate_z_uncertainty_GAMA()

Note that the uncertainty is returned in km/s.



### Refining fits ###

In some cases, the fit is sub-optimal because it could be either Ly-a
or [O II] for instance - this happens relatively frequently. To check
for this there is a function `update_solution_for_aliasing_muse`:

	r.update_solution_for_aliasing_muse(verbose=True)

The action of this routine is to first check whether the *best*
redshift has an equivalent among the other three peaks that
corresponds to the same feature in the observed spectrum but a
different emission line identified (`check_for_aliasing_peaks_muse`
does the work).

If this is the case, then it loops over these aliasing matches and fit
three possible line solutions to the feature: a double-exponential fit
to Ly-a, and a Gaussian double fit to C III] and [O II]. The reduced
chi squares are compared and the lowest is taken to be the best fit. 

This function does not have output, but it updates the `z_peak` value
(and associated quantities) if the best redshift is found to be better
determined by another solution. 

To visualise this process it can be useful to call the routine
directly:

	r.update_solution_for_aliasing_muse(verbose=True, plot=True)

or even the core routine directly:

	info = r.fit_possible_emlines(verbose=True, plot=True, chisq_window=500)

This latter function takes an additional argument `l_feature` which
you can use to zoom in on a particular feature.


### Visualising results ###

The two last functions introduced above do create plots. In addition
the `show_one_solution` routine is useful:

	fig = r.show_one_solution(0)

which shows the best solution (when the argument is 0) or any other
solution.