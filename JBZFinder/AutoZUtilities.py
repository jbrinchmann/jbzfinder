import sys
import numpy as np
from astropy.convolution import convolve, Box1DKernel
import scipy.signal as sps
from scipy.optimize import curve_fit
from astropy.io import fits
from astropy.table import Table
from scipy.optimize import leastsq
import matplotlib.pyplot as plt
#from JBMath.PolynomialFits import poly_reject

try:
    import JBZFinder.AutoZ as az
except:
    print "Template file creation not available!"

import numpy.fft as fft
#import pdb


# For fitting we also want to try mpfit and lmfit. 
# but it is possible the user has none of these in
# which case we will complain mildly when the fitting is 
# done.
try:
    from mpfit import mpfit
    have_mpfit = True
except:
    have_mpfit = False

try:
    from lmfit import minimize, Parameters
    have_lmfit = True
except:
    have_lmfit = False






def gauss(x, *p):
    A, mu, sigma, lvl = p
    return lvl + A*np.exp(-(x-mu)**2/(2.*sigma**2))

def gaussfit(x, y, p_initial=None):
    """
    Very simple fit of a Gaussian to data
    """

    if p_initial is None:
        p_initial = [np.max(y), np.median(x), 0.1*(np.max(x)-np.min(x)), 0.0]
    

    coeff, var_matrix = curve_fit(gauss, x, y, p0=p_initial)

    return coeff, var_matrix, gauss(x, coeff)


def double_exponential(x, *p):
    """
    Calculate a simple double exponential

    f(x) = A exp(-(l-l_c)/h1)*(I(l<l_c)) + A exp(-(l_c-l)/h2)*I(l>l_c) + level

    Parameter order:
    p[0] : A
    p[1] : l_c
    p[2] : h1
    p[3] : h2
    p[4] : level

    The integral of this over +/- Infinity is A*(h1+h2)
    
    """

    A, l_c, h1, h2, lvl = p

    y = np.zeros_like(x)
    i_left, = np.where(x < l_c)
    i_right, = np.where(x >= l_c)
    y[i_left] = A*np.exp(-(l_c-x[i_left])/h1)
    y[i_right] = A*np.exp(-(x[i_right]-l_c)/h2)

    return y + lvl
    

def fit_double_exponential(restwl, flux, dflux, l_c, dw=20):
    """
    Try to fit a particular emission line with double exponential
    """
    
    # Extract a region around the emission line.
    use, = np.where(np.abs(restwl - l_c) < dw)
    x = restwl[use]
    y = flux[use]
    dy = dflux[use]

    # Determine initial conditions.
    i_peak = np.argmin(np.abs(x-l_c))
    a_guess = np.max(y[i_peak-2:i_peak+2])
    l_c_guess = l_c
    p_initial = [a_guess, l_c_guess, 1.0, 1.0, np.median(y)]
    
    coeff, var_matrix = curve_fit(double_exponential, x, y, p0=p_initial,
                                  sigma=dy)

    return coeff, var_matrix, double_exponential(restwl, *list(coeff))


def double_exp_func(x, A, l_c, h1, h2, lvl):

    y = np.zeros_like(x)
    i_left, = np.where(x < l_c)
    i_right, = np.where(x >= l_c)
    y[i_left] = A*np.exp(-(l_c-x[i_left])/h1)
    y[i_right] = A*np.exp(-(x[i_right]-l_c)/h2)

    return y+lvl

def double_exp_model_mpfit(p, x=None, y=None, dy=None, l_c=None, fjac=None):
    """
    An interface function for double_gauss_model to use with mpfit

    mpfit does not allow for extra arguments to be passed, but does
    support keyword arguments. This routine uses that interface and 
    then calls double_gauss_model
    """

    return double_exp_model(p, y, dy, x, l_c, 1)

def double_exp_model(p, y, dy, x, l_c, return_type):
    """
    Calculate a simple double exponential

    f(x) = A exp(-(l-l_c)/h1)*(I(l<l_c)) + A exp(-(l_c-l)/h2)*I(l>l_c) + level

    Parameter order:
    p[0] : A
    p[1] : delta lambda
    p[2] : h1
    p[3] : h2
    p[4] : level

    The integral of this over +/- Infinity is A*(h1+h2)
    
    """

    if type(p) is list or type(p) is np.ndarray:
        # Normal scipy.optimize.leastsq call
        # This way of ordering the logic avoids problems if
        # lmfit is not available.
        A, d_lambda, h1, h2, lvl = p
    else:
        A = p['A'].value
        d_lambda = p['shift'].value
        h1 = p['h1'].value
        h2 = p['h2'].value
        lvl = p['level'].value

    y_fit = double_exp_func(x, A, l_c+d_lambda, h1, h2, lvl)

    err = (y-y_fit)/dy

    if return_type == 1:
        # For mpfit
        return [0, err]
    else:
        return err

        

def double_gauss_func(x, a1, a2, mu1, mu2, sigma, level):
    
    y_fit = (gauss(x, a1, mu1, sigma, 0.0) + 
             gauss(x, a2, mu2, sigma, 0.0 ) + level)

    return y_fit
    

def double_gauss_model_mpfit(p, x=None, y=None, dy=None, l_c1=None, l_c2=None, fjac=None):
    """
    An interface function for double_gauss_model to use with mpfit

    mpfit does not allow for extra arguments to be passed, but does
    support keyword arguments. This routine uses that interface and 
    then calls double_gauss_model
    """

    return double_gauss_model(p, y, dy, x, l_c1, l_c2, 1)
    
    
def double_gauss_model(p, y, dy, x, l_c1, l_c2, return_type):
    #
    # A simple double-Gaussian model
    # 
    # p[0] = Amplitude 1
    # p[1] = Amplitude 2
    # p[2] = sigma
    # p[3] = level
    # p[4] = wavelength shift
    #

    if type(p) is list or type(p) is np.ndarray:
        # Normal scipy.optimize.leastsq call
        # This way of ordering the logic avoids problems if
        # lmfit is not available.
        a1, ra2, sigma, lvl, dl = p
    else:
        a1 = p['A1'].value
        ra2 = p['rA2'].value
        sigma = p['sigma'].value
        lvl = p['level'].value
        dl = p['shift'].value

    a2 = a1/ra2
    
    m1 = l_c1 + dl
    m2 = l_c2 + dl
        
    y_fit = double_gauss_func(x, a1, a2, m1, m2, sigma, lvl)
    err = (y - y_fit)/dy

    if return_type == 1:
        # This is for mpfit..
        return [0, err]
    else:
        return err



def fit_doublet(restwl, flux, dflux, l_c1, l_c2, dw=20, use_lmfit=False, use_mpfit=False):
    """
    Try to fit a particular emission line with double Gaussian

    Arguments:
    ----------
    restwl: ndarray
       The rest-frame wavelength of the spectrum
    flux: ndarray
       The flux in each bin
    dflux: ndarray
       The uncertainty on the flux
    l_c1: float
       The rest-wavelength of the first line
    l_c2: float
       The rest-wavelength of the second line

    Keywords:
    ---------
    use_lmfit: bool
       If set to true, use [lmfit](https://lmfit.github.io/lmfit-py/index.html) to 
       do the minimization. In this case the parameter values are
       constrained, see the discussion section below. Default: False
    use_mpfit: bool
       If set to true, use [mpfit](https://lmfit.github.io/lmfit-py/index.html) to 
       do the minimization. In this case the parameter values are
       constrained, see the discussion section below. Default: False

    Returns:
    --------
    coeff: list or ndarray
       The best-fit parameters. The order of the parameters is:
          0: Amplitude of Gaussian 1
          1: Ratio of Gaussian 2 to Gaussian 1
          2: Sigma of the Gaussians
          3: The zero level
          4: The wavelength shift applied for the best-fit.
    covar: ndarray
       The co-variance matrix of the parameters.
    y_est: ndarray
       The best-fit estimate of y.
    
    Discussion:
    -----------
    This routine supports three different minimization codes. The default
    is to use scipy.optimize.leastsq but this does not support limits on
    the parameters. For that reason the user can use lmfit or mpfit 
    instead.

    When lmfit or mpfit is chosen, some parameters are constrained. These are:
    
       Amplitudes of Gaussians: Constrained to be non-negative
       Sigma of the Gaussians: Constrained to be larger than 0.05 Angstrom
                            with a maximum value of 300.0 km/s
       Shift of the lines: Constrained to be within +/- 500 km/s


    Fitted function:
    ----------------
    The actual function fit is double_gauss_func.  Given the 
    coeff array one can recover the best fit using:

     double_gauss_funct(x, *coeff)
    """

    # Extract a region around the emission line.
    use, = np.where(np.abs(restwl - 0.5*(l_c1+l_c2)) < dw)
    x = restwl[use]
    y = flux[use]
    dy = dflux[use]
    
    # Determine initial conditions.
    i_peak1 = np.argmin(np.abs(x-l_c1))
    i_peak2 = np.argmin(np.abs(x-l_c2))
    a1_guess = np.max(y[i_peak1-2:i_peak1+2])
    a2_guess = np.max(y[i_peak2-2:i_peak2+2])

    # Ensure that the amplitude guesses are >0 and we also 
    # take them to be equal
    if a1_guess < 0:
        a1_guess = 0
    if a2_guess < 0:
        a2_guess = 0
    tmp = 0.5*(a1_guess+a2_guess)
    if tmp <= 0:
        a1_guess = 0.1
        a2_guess = 0.1
    else:
        a1_guess = tmp
        a2_guess = tmp

    ra2_guess = a2_guess/a1_guess # Should be 1
    sigma_guess=1.0
    level_guess = np.median(y)
    shift_guess = 0.0

    # If the fit is constrained, these limits are also used.
    max_sigma_vel = 300.0 # km/s
    max_shift_vel = 500.0 # km/s
    cspeed = 2.99792e5      # km/s
    max_shift = 0.5*(l_c1+l_c2)*max_shift_vel/cspeed
    max_sigma = 0.5*(l_c1+l_c2)*max_sigma_vel/cspeed


    if use_lmfit:
        # Check that we have lmfit.
        if not have_lmfit:
            print "You do not have lmfit installed - please install this first!"
            return None

        p = Parameters()
        p.add('A1', value=a1_guess)
        p.add('rA2', value=ra2_guess)
        p.add('sigma', value=sigma_guess)
        p.add('level', value=level_guess)
        p.add('shift', value=shift_guess)

        # Add constraints. Non-negative fluxes.
        # And the flux ratio of the [O II] lines or the C III] lines should not be too large.
        p['A1'].min = 0.0
        p['rA2'].min = 0.25
        p['rA2'].max = 4.0

        # Positive line width and not too large.
        p['sigma'].min = 0.05
        p['sigma'].max = max_sigma
        p['shift'].min = -max_shift
        p['shift'].max = max_shift

        out = minimize(double_gauss_model, p, args=(y, dy, x, l_c1, l_c2, 0))

        v = out.params.valuesdict()
        coeff = [v['A1'], v['rA2'], v['sigma'], v['level'], v['shift']]
        pcov = out.covar
        
    else:
        # Appropriate for both mpfit and leastsq
        p = [a1_guess, ra2_guess, sigma_guess, level_guess, shift_guess]

        
        # Decide what fitting code to use.
        if use_mpfit:
            # Check that we have mpfitx
            if not have_mpfit:
                print "You do not have mpfit installed - please install this first!"
                return None
        

            p = np.array(p, dtype=np.float64)
            
            # Create the parinfo structure.
            parinfo = [{'value':0., 'fixed':0, 'limited':[0,0], 'limits':[0.,0.]} for i in p]

            # Limit the amplitudes to be non-negative
            parinfo[0]['limited'] = [1, 0]
            parinfo[0]['limits'] = [0.0, 1e30]
            parinfo[1]['limited'] = [1, 1]
            parinfo[1]['limits'] = [0.25, 4.0]

            # Limit the sigma to e >0.05 and not too large
            parinfo[2]['limited'] = [1, 1]
            parinfo[2]['limits'] = [0.05, max_sigma]

            # Limit the shift within +/- max_shift
            parinfo[4]['limited'] = [1, 1]
            parinfo[4]['limits'] = [-max_shift, max_shift]

            # We also need to put the data into a dictionary.
            data = {'x': x, 'y': y, 'dy': dy, 'l_c1': l_c1, 'l_c2': l_c2}
            
            out = mpfit(double_gauss_model_mpfit, p, parinfo=parinfo, functkw=data, quiet=True)
            coeff = out.params
            pcov = out.covar
        else:
            coeff, pcov, info, msg, ierr = leastsq(double_gauss_model, p, 
                                                   args = (y, dy, x, l_c1, l_c2, 0),
                                                   full_output=True)
            
    # Finally calculate the model fit
    dl = coeff[4]
    y_est = double_gauss_func(restwl, coeff[0], coeff[0]/coeff[1], l_c1+dl, l_c2+dl, coeff[2], coeff[3])

    if use_lmfit:
        result = (coeff, pcov, y_est, out)
    elif use_mpfit:
        result = (coeff, pcov, y_est, out)
    else:
        result = (coeff, pcov, y_est)
        

    return result


def fit_double_exp(restwl, flux, dflux, l_c, dw=20, use_lmfit=False, use_mpfit=False):
    """
    Try to fit a particular emission line with a double Exponential

    Arguments:
    ----------
    restwl: ndarray
       The rest-frame wavelength of the spectrum
    flux: ndarray
       The flux in each bin
    dflux: ndarray
       The uncertainty on the flux
    l_c: float
       The rest-wavelength of the line

    Keywords:
    ---------
    dw: float
       Only the spectrum within dw of the line will be used for the fit.
       Default: 20.
    use_lmfit: bool
       If set to true, use [lmfit](https://lmfit.github.io/lmfit-py/index.html) to 
       do the minimization. In this case the parameter values are
       constrained, see the discussion section below. Default: False
    use_mpfit: bool
       If set to true, use [mpfit](https://lmfit.github.io/lmfit-py/index.html) to 
       do the minimization. In this case the parameter values are
       constrained, see the discussion section below. Default: False

    Returns:
    --------
    coeff: list or ndarray
       The best-fit parameters. The order of the parameters is:
          0: Amplitude of combined line
          1: Central wavelength shift
          2: Scale-length of the left exponential
          3: Scale-length of the right exponential
          4: The zero-level
    covar: ndarray
       The co-variance matrix of the parameters.
    y_est: ndarray
       The best-fit estimate of y.
    
    Discussion:
    -----------
    This routine supports three different minimization codes. The default
    is to use scipy.optimize.leastsq but this does not support limits on
    the parameters. For that reason the user can use lmfit or mpfit 
    instead.

    When lmfit or mpfit is chosen, some parameters are constrained. These are:
    
       Amplitudes of Line: Constrained to be non-negative
       Scale-lengths: Constrained to be larger than 0.05 Angstrom
                            with a maximum value of 500.0 km/s
       Shift of the line: Constrained to be within +/- 500 km/s


    Fitted function:
    ----------------
    The actual function fit is double_gauss_func.  Given the 
    coeff array one can recover the best fit using:

     double_exp_func(x, *coeff)
    """

    # Extract a region around the emission line.
    use, = np.where(np.abs(restwl - l_c) < dw)
    if len(use) == 0:
        return None
    x = restwl[use]
    y = flux[use]
    dy = dflux[use]
    
    # Determine initial conditions.
    i_peak = np.argmin(np.abs(x-l_c))
    a_guess = np.max(y[i_peak-2:i_peak+2])
    shift_guess = 0.0

    # Ensure that the amplitude guess is >0
    if a_guess < 0:
        a_guess = 0.01
    h1_guess = 0.9
    h2_guess = 0.9
    level_guess = np.median(y)

    # If the fit is constrained, these limits are also used.
    max_h_vel = 500.0 # km/s
    max_shift_vel = 500.0 # km/s
    cspeed = 2.99792e5      # km/s
    max_shift = 0.5*l_c*max_shift_vel/cspeed
    max_h = 0.5*l_c*max_h_vel/cspeed


    if use_lmfit:
        # Check that we have lmfit.
        if not have_lmfit:
            print "You do not have lmfit installed - please install this first!"
            return None

        p = Parameters()
        p.add('A', value=a_guess)
        p.add('shift', value=shift_guess)
        p.add('h1', value=h1_guess)
        p.add('h2', value=h2_guess)
        p.add('level', value=level_guess)

        # Add constraints. Non-negative flux.
        p['A'].min = 0.0

        # Positive line width and not too large.
        p['h1'].min = 0.05
        p['h1'].max = max_h
        p['h2'].min = 0.05
        p['h2'].max = max_h

        # And constrain the shift
        p['shift'].min = -max_shift
        p['shift'].max = max_shift

        out = minimize(double_exp_model, p, args=(y, dy, x, l_c, 0))

        v = out.params.valuesdict()
        coeff = [v['A'], v['shift'], v['h1'], v['h2'], v['level']]
        pcov = out.covar
        
    else:
        # Appropriate for both mpfit and leastsq
        p = [a_guess, shift_guess, h1_guess, h2_guess, level_guess]

        
        # Decide what fitting code to use.
        if use_mpfit:
            # Check that we have mpfitx
            if not have_mpfit:
                print "You do not have mpfit installed - please install this first!"
                return None
        

            p = np.array(p, dtype=np.float64)
            
            # Create the parinfo structure.
            parinfo = [{'value':0., 'fixed':0, 'limited':[0,0], 'limits':[0.,0.]} for i in p]

            # Limit the amplitude to be non-negative
            parinfo[0]['limited'] = [1, 0]
            parinfo[0]['limits'] = [0.0, 1e30]

            # Limit the shift within +/- max_shift
            parinfo[1]['limited'] = [1, 1]
            parinfo[1]['limits'] = [-max_shift, max_shift]
            

            # Limit the scale-lengths to be non-negative and reasonable.
            parinfo[2]['limited'] = [1, 1]
            parinfo[2]['limits'] = [0.05, max_h]
            parinfo[3]['limited'] = [1, 1]
            parinfo[3]['limits'] = [0.05, max_h]


            # We also need to put the data into a dictionary.
            data = {'x': x, 'y': y, 'dy': dy, 'l_c': l_c}
            
            out = mpfit(double_exp_model_mpfit, p, parinfo=parinfo, functkw=data, quiet=True)
            coeff = out.params
            pcov = out.covar
        else:
            coeff, pcov, info, msg, ierr = leastsq(double_exp_model, p, 
                                                   args = (y, dy, x, l_c, 0),
                                                   full_output=True)
            
    # Finally calculate the model fit
    dl = coeff[1]
    y_est = double_exp_func(restwl, coeff[0], l_c+dl, coeff[2], coeff[3], coeff[4])

    if use_lmfit:
        result = (coeff, pcov, y_est, out)
    elif use_mpfit:
        result = (coeff, pcov, y_est, out)
    else:
        result = (coeff, pcov, y_est)
        

    return result
    

def poly_reject(xin, yin, degree, high=5.0, low=5.0, yerr=None, niterate=3,
                verbose=True):
    """
    Calculate  polynomial fit with a given degree to x & y with sigma rejection of outliers

    Parameters:
       x: float array
         The x-values of the data.
       y: float array
         The y-values of the data.
  degree: int
         The maximum degree of the polynomial fit.
    high: float
         The upper sigma cut-off. Default to 5.
    low: float
         The lower sigma cut-off. Default to 5.
   yerr: float array
         The uncertainty on the y values. Default to None.
niterate: int
         The number of iteration steps to take.

    Operation:

    The idea is that the input values are y = poly(x) + noise, contaminated
    by outliers. We will then define an outlier as having a large residual,
    where residual > high*sigma where sigma are the studentized residuals 
    when yerr is provided.
    
    """

    # Start with converting the input to float64 to avoid instability in 
    # poly_fit...
    x = np.float64(xin)
    y = np.float64(yin)
    
    done = False
    residuals = y*0.0 

    if yerr is not None:
        w = 1.0/yerr**2
        print "Warning: The weighted rejection fit has not been tested and might be buggy!"
    else:
        # Slightly faster than setting w to 1.
        w = None

    i_iterate = 1
    
    while not done:

        # Find ok points.
        ok, = np.where((residuals < high) & (residuals > -low))
        # Run the fit.
        if w is not None:
            coeffs = np.polyfit(x[ok], y[ok], degree, w=w[ok])
        else:
            coeffs = np.polyfit(x[ok], y[ok], degree)

        # Calculate studentized residuals.
        p = np.poly1d(coeffs)
        yfit = p(x)
        residuals = (y - yfit)
        if yerr is not None:
            residuals /= yerr
        else:
            stdev_calc = np.std(residuals[ok])
            residuals /= stdev_calc

        # print "Iteration {0:2d}:  Stdv: {1}".format(i_iterate, stdev_calc)
        #        pdb.set_trace()


        # Check whether we are done.
        bad, = np.where((residuals[ok] > high) | (residuals[ok] < -low))
        if (len(bad) == 0) or (i_iterate == niterate):
            done = True

        i_iterate += 1


    # Prepare the output result.
    n_rejected = len(x)-len(ok)
    rejected, = np.where((residuals >= high) | (residuals <= -low))
    info = {'yfit': yfit, 'n_rejected': n_rejected, 'ok': ok,
            'rejected': rejected}

    return coeffs, info


def set_drs_location(DIR):
    """
    Set the location of the DRS.

    Arguments:
    ----------

    DIR: str
       The root direction of the DRS. 

    Example:
       azu.set_drs_location('/data2/jarle/MUSE/Software/DRS/muse-kit-0.16.0/muse-calib-0.16.0/')

    """

    global __DRSDIR__
    __DRSDIR__ = DIR

    
def get_sky_spectrum(skyfile=None):
    """
    Load a sky spectrum

    Keywords:
    ---------
    skyfile: str
       Set to the name of the skyfile to use

    Procedure:
    ----------
    If no skyfile is provided, the code attempts to find the sky
    file from the DRS. To get this work, the location of the DRS 
    needs to be set.
    
    """

    if skyfile is None:
        # Check for the DRS.
        if __DRSDIR__ is None:
            print "get_sky_spectrum: If you do not provide the sky file"
            print "   you need to call set_drs_location to set the location"
            print "   of the DRS."
            return None
    
        skyfile = __DRSDIR__+'cal/sky_lines.fits'

    hdul = fits.open(skyfile)
    t = hdul[1].data
    hdul.close()

    return t

def normalise_aad(y, clipvalue=None, indices=None, fraction_to_keep=0.95,
                      center='median'):
    """
    Normalise vector y by dividing by the average absolute deviation, optionally
    iteratively clipping outliers.
    
    Parameters
    ----------
    y : ndarray
       The array to normalise (usually a spectrum)
       
    clipvalue : float
       The factor to multiply the mean absolute deviation by before clipping (see notes).
       If set to None, no clipping is applied. Default: None
       
    indices : ndarray or boolean array
       The indices of the points in y to use when calculating the mean
       absolute deviation. When set to None, the fraction_to_keep value
       determines the fraction of the points to use. The default is None.

    fraction_to_keep: float
       The fraction of points in y to keep for the calculation of the mean absolute deviation

    center : string
       How to determine the center. Valid values: 'median' uses the median of
       the valid points, 'mean' uses the average, 'none' does not subtract off
       a central value (y must then be centred on zero from before)
       
    Returns:
    --------
    yclip : ndarray
       The normalised spectrum.

    Notes:
    ------
    The basic calculation is AAD = Avg( |spec-M(spec)|) where M(spec) is determined
    by the center keyword. Since the main operation is on spectra whose pixel distribution
    can be rather skewed due to strong emission lines, not all points are used by defadsult.

    Instead only points in 'indices' are used if that is passed, and if not, then
    the 1-fraction_to_keep highest pixels are excluded from the AAD calculation. 

    The clipping is implemented in a simple way by capping the values at each iteration step
    In this case the input spectrum is also modified beyond the normalisation.
    
    """

    # The centring function.
    Mlookup = {'none': lambda x: 0.0,
               'median': lambda x: np.median(x),
               'mean': lambda x: np.mean(x)}
    try:
        Mc = Mlookup[center.lower()]
    except KeyError:
        print "Centering method {0} is not known. I will continue with a zero center!".format(center.lower())
        Mc = Mlookup['none']
        

    if indices is None:
        if fraction_to_keep < 1.0:
            ok = np.where(np.isfinite(y))

            # We need the central value
            yc = y - Mc(y[ok])
                
            # Find the fraction lowest values.
            abs_yc = np.abs(yc)
            level = np.percentile(abs_yc[ok], 100*fraction_to_keep)

            indices, = np.where(abs_yc <= level)

        else: 
            # Keep all.
            indices = np.arange(len(y))

    centre_value = Mc(y[indices])

    # This is the result output - I 
    y_out = y.copy()-centre_value
    
    if clipvalue is None:
        aad = np.mean(np.abs(y_out[indices]))
    else:
        # Iterate the calculation.
        cliptest = clipvalue + 0.01
        keep_clipping = True
        while keep_clipping:
            aad = np.mean(np.abs(y_out[indices]))
            if np.nanmax(y_out) <= cliptest*aad:
                keep_clipping = False
            y_out = np.clip(y_out, -clipvalue*aad, clipvalue*aad)

    # Add back the central value we subtracted off above.
    y_out += centre_value 
    return y_out/aad, aad, centre_value, indices
                

def vactoair(vacwl):
    """
    Calculate the approximate wavelength in air for vacuum wavelengths

    Parameters
    ----------
    vacwl : ndarray
       Vacuum wavelengths.
    
    This uses an approximate formula from the IDL astronomy library
    vactoair.pro.
    """

    wave2 = vacwl*vacwl
    n = 1.0 + 2.735182e-4 + 131.4182/wave2 + 2.76249e8/(wave2*wave2)

    # Do not extrapolate to very short wavelengths.
    if not isinstance(vacwl, np.ndarray):
        if vacwl < 2000:
            n=1.0
    else:
        ignore = np.where(vacwl < 2000)
        n[ignore] = 1.0

    return vacwl/n


def airtovac(airwl):
    """
    Convert air wavelengths to vacuum wavelengths

    Parameters
    ----------
    vacwl : ndarray
       Vacuum wavelengths.
    
    This uses the IAU standard as implemented in the IDL astronomy
    library airtovac.pro
    """

    sigma2 = (1e4/airwl)**2.        # Convert to wavenumber squared
    n = 1.0 + (6.4328e-5 + 2.94981e-2/(146. - sigma2) +
               2.5540e-4/( 41. - sigma2))
    
    if not isinstance(airwl, np.ndarray):
        if airwl < 2000:
            n=1.0
    else:
        ignore = np.where(airwl < 2000)
        n[ignore] = 1.0

    return airwl*n


def median_filter_autoz(y, width, mimic_idl=False):
    """
    Implement median filtering of input spectrum.

    The default IDL implementation of the median filter does not handle edges 
    gracefully so AutoZ uses a modified routine. The exact operation of this
    is reproduced if mimic_idl is set to True. Otherwise we use the edge behaviour
    of scipy.signal.medfilt which is quite similar.
    """

    y_smooth = sps.medfilt(y, width)
    if mimic_idl:
        # Follow median_adjust.pro.

        N = len(y)
        i1 = width/2 - 1 if width > 1 else 0
        i2 = width/2 + 1

        y_smooth[0:i1+1] = np.median(y[0:i2+1])
        y_smooth[N-1-i1:N] = np.median(y[N-1-i2:N])


    return y_smooth


def med_smooth(y, median_width, boxcar_width, mimic_idl=False):
    """
    Median filter and then smooth an array y.

    Based on med_smooth.pro by Ivan Baldry.
    """

    # First run the median filter.
    y_smooth = median_filter_autoz(y, median_width, mimic_idl=mimic_idl)

    # Then smooth using a boxcar filter.
    y_smooth = convolve(y_smooth, Box1DKernel(boxcar_width), boundary='extend')

    return y_smooth


def make_sky_mask(sky, wavelength, cut=10, dl=1.25):
    """
    Create a mask of sky lines

    Arguments:
    ----------
    sky: recarray or dict
       The sky spectrum, as returned from get_sky_spectrum.
    wavelength: ndarray
       The array of wavelengths you want the mask to correspond
       to.

    Keywords:
    ---------
    cut: float
       The cut level for the sky spectrum - lines below this
       are ignored when constructing the mask. Default=10.
    dl: float
       The wavelength range around each sky line that is masked
       out. Default=1.25
    
    """

    mask = np.zeros_like(wavelength, dtype=np.int8)
    
    use, = np.where(sky['flux'] > cut)
    if len(use) == 0:
        print "No line!"
        return mask
    else:
        xl = sky['lambda'][use]
        yl = sky['flux'][use]

    # The sky mask is created using a loop which is sub-optimal
    # in the case of regular wavelength sampling but this should
    # work in most cases.
    for i in range(len(xl)):
        affected, = np.where(np.abs(xl[i]-wavelength) < dl)
        mask[affected] = 1
        
    
    return mask
    

    

def apodize_signal(x, y, start, end, offset1, offset2):
    """
    Implement the apodization of the spectrum defined by AutoZ

    Calculates a simple cosine bell taper at the edges given by the offsets. 
    """


    left, = np.where((x > start + offset1) & (x <= start + offset2))
    N = len(left)
    y_ap = y.copy()
    if N > 2:
        #        print "Apodizing on the left {0}".format(N)
        phase = np.pi*np.arange(N)/(N-1)
        window = 0.5*(1.0-np.cos(phase))
        y_ap[left] = y_ap[left]*window

    right, = np.where((x > end - offset2) & (x <= end - offset1))
    N = len(right)
    if N > 2:
        #        print "Apodizing on the right {0}".format(N)
        phase = np.pi*np.arange(N)/(N-1)
        window = 0.5*(1.0+np.cos(phase))
        y_ap[right] = y_ap[right]*window

    # Finally set the edges to zero.
    left = np.where(x <= start+offset1)
    y_ap[left]=0.0

    right = np.where(x >= end-offset1)
    y_ap[right]=0.0
        
    return y_ap


def trimmed_mean(x, frac_or_num, sorted=False, debug=False):
    """
    Calculate the trimmed mean of x excluding either a fraction or a certain number.

    Parameters:
    -----------
    
    x: ndarray
     The data for which a trimmed mean is requested.
    frac_or_num: float or int
     If this number is <1 then it is taken to be the fraction of
     points to trim off. These are taken symmetrically from the 
     two sides fo the array. If it is >=1,
     then it is interpreted as a number of points to trim off.

    Keywords:
    ---------

    sorted: bool
     If set to true, then the input value x, is assumed to be
     sorted so no sorting is done.

    Notes:
    -------
    This routine gives slightly different results from the AutoZ
    mean_reject function when the number to reject is an odd number.  
    """

    N = len(x)
    if sorted:
        inds = np.arange(N)
    else:
        inds = np.argsort(x)

    if frac_or_num < 1:
        # Fraction.
        N_to_cut = np.round(N*frac_or_num)
    else:
        N_to_cut = frac_or_num

    N_left = np.round(N_to_cut/2.)
    N_right = N_to_cut - N_left

    
    x_calc = x[inds[N_left:N-N_right]]

    if debug:
        print "I want to reject {0}, which means {1} on the left and {2} on the right".format(N_to_cut, N_left, N_right)
        print "I am using ", x[inds]
        print "I am keeping :", x_calc

    return np.mean(x_calc)
        
    

def cross_correlate(template, spec, corr_size):
    """
    Carry out cross-correlation of a template with a spectrum.

    This is based on the IDL AutoZ cross-correlation function. There is 
    however a slight difference because of the way the transforms are 
    normalised in the IDL code.
    """

    # First calculate the conjugate of the Fourier transform of
    # the rebinned template spectrum.
    fft_t = fft.fft(template)
    fft_t = np.conj(fft_t)

    # Fourier transform the actual spectrum.
    fft_spec = fft.fft(spec)

    #    pdb.set_trace()
    
    # Multiply by the conjugate F(template) from above
    fft_spec *= fft_t

    # We want the real part of the inverse FFT.
    inv_fft = fft.ifft(fft_spec)
    cr_c = np.real(inv_fft)

    # Shift the cross-correlation function to have zero at
    # the middle.
    n_shift = len(cr_c)/2
    cr_c = np.roll(cr_c, n_shift)

    # Create the output array.
    cr_corr = cr_c[n_shift-corr_size:n_shift+corr_size+1]

    return cr_corr

    
def find_peaks(y):
    """
    Find local peaks in an array.

    Modeled on the AutoZ criteria_peak routine. The code simply compares pixels
    with their neighbours.
    """

    N = len(y)
    results = np.zeros(N, dtype=bool)

    is_peak = (y[1:-1] >= y[0:-2]) & (y[1:-1] > y[2:])
    results[1:N-1] = is_peak

    return results



##############################
# Template related functions
##############################

def load_template_file(filename):
    """
    Read in templates for cross-correlation.

    Attributes:
     filename: str
       The name of the file to read from. 

    Returns:
      tdata: The template binary table
      rms_z_range: The redshift range used for error calculation
      allowed_z_range: The redshift range allowed for solutions 
                    for this template.
    
    The original format (used in the IDL code) is to read
    one fits binary table with all templates in one array.
    This is not necessarily the optimal solution in the long
    run. A better solution would probably be to have multiple
    extensions and one template per extension.

    The routine also returns the rms_z_range and allowed_z_range. These 
    are crucial variables for the processing of the spectra. There are
    two ways these are specified: if the FITS file has three extensions,
    the rms_z_range is expected to be stored in the second and the 
    allowed_z_range in the third (TODO: The format of the template file
    should be revamped). If this is not the case, it is assumed that the 
    templatefile follows a fixed format copied from the IDL AutoZ code.

    Example: 
       import AutoZUtilities as azu
       tdata, rms_z_range, allowed_z_range = azu.load_template_file(filename)

    """

    try:
        hdul = fits.open(filename)
        tdata = hdul[1].data
        if len(hdul) > 2:
            rms_z_range = hdul[2].data
        else:
            rms_z_range = None
        if len(hdul) > 3:
            allowed_z_range = hdul[3].data
        else:
            allowed_z_range = None
        hdul.close()
    except IOError: 
        print "I failed opening the template file "+filename
        return None
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise

    if rms_z_range is not None and allowed_z_range is None:
        print "A template file cannot contain RMS_z_range and no allowed_z_range..."
        return None

    # The RMS is the "redshift" range over which the noise level
    # in the cross-correlation function is evaluated. This follows 
    # somewhat Heavens (1993). If this is not provided in the 
    # template file, then we use the default AutoZ setup. This 
    # uses hardcoded template numbers. 

    if rms_z_range is None:
        rms_z_range = np.zeros((2, len(tdata)))
        allowed_z_range = np.zeros((2, len(tdata)))

        # Default stellar setup - we overwrite late-types next for RMS.
        rms_z_range[:, 0:23] = np.array([-0.1, 0.5])[:, None]
        allowed_z_range[:, 0:23] = np.array([-0.002, 0.002])[:, None]

        # Late stars with power at the red end.
        i_late_stars = [11, 12, 13, 14, 15, 17, 19, 22]
        rms_z_range[:, i_late_stars] = np.array([-0.2, 0.4])[:, None] 

        # Original galaxy templates.
        rms_z_range[:, 23:29] = np.array([-0.1, 0.8])[:, None]
        allowed_z_range[:, 23:29] = np.array([-0.005, 0.800])[:, None]

        # intermediate-z templates. Check that these are ok.
        rms_z_range[:, 29:33] = np.array([-0.1, 1.6])[:, None]
        allowed_z_range[:, 29:33] = np.array([-0.005, 1.6])[:, None]

        # MUSE high-z galaxy templates
        rms_z_range[:, 33:] = np.array([2.6, 6.0])[:, None]
        allowed_z_range[:, 33:] = np.array([2.8, 6.0])[:, None]

    return tdata, rms_z_range, allowed_z_range
    

def add_spectrum_to_template_file(wavelength, flux,oldfile, newfile,
                                  label='Manually added', redshift=0.0,
                                  description='N/A', 
                                  dflux=None, rms_region=None, allowed_region=None):
    """
    Add a template spectrum to a template file using the AutoZ format.
    """

    if dflux is None:
        dflux = flux*0.0+np.median(flux)*0.1

    rest = None
    
    if rms_region is None:
        print "It is advisable that you provide a RMS region - I will try to guess for MUSE.."
        rest = wavelength/(1+redshift)
        # MUSE standard covers 4750-9300, so I will use that as my 
        # reference and require at least 100 A overlap
        z_min = (4750+100)/np.max(rest)-1.0
        z_max = (9300-100)/np.min(rest)-1.0

        print "The minimum z={0} corresponding to l_max={1}".format(z_min, np.max(rest))
        print "The maximum z={0} corresponding to l_min={1}".format(z_max, np.min(rest))
        if z_min < 0:
            z_min = 0.0
        rms_region = [z_min-0.1, z_max+0.2]

    if allowed_region is None:
        print "It is advisable that you provide a allowed z region - I will try to guess for MUSE.."
        if rest is None:
            rest = wavelength/(1+redshift)
            # MUSE standard covers 4750-9300, so I will use that as my 
            # reference.
            z_min = (4750+500)/np.max(rest)-1.0
            z_max = (9300-500)/np.min(rest)-1.0

            print "The minimum z={0} corresponding to l_max={1}".format(z_min, np.max(rest))
            print "The maximum z={0} corresponding to l_min={1}".format(z_max, np.min(rest))
            if z_min < 0:
                z_min = 0.0
        allowed_region = [z_min, z_max]
        
            
        
        
    # Read the template file
    tdata, rms_z_range, allowed_z_range = load_template_file(oldfile)

    # For ease of extension I find it tidiest to use astropy Table.
    t = Table(tdata)

    # Check whether this template file has description entries 
    if 'DESCRIPTION' in t.colnames:
        has_desc = True
    else:
        has_desc = False
        

    # Find the maximum template number and add one.
    template_number = np.max(t['TEMPLATE_NUM'].data)+1

    # Create a filtered spectrum
    fz = az.AutoZ()
    fz.start_lambda = np.min(wavelength)
    fz.end_lambda = np.max(wavelength)
    spec_filt, info = fz.process_spectrum(flux, dflux, wavelength, mimic_idl=True)
    
    # Create the new spectrum.
    n_lambda_t = len(t['LAMBDA'][0])
    n_lambda_in = len(flux)

    i_start=0
    i_end = np.min([n_lambda_t, n_lambda_in])
    if n_lambda_in > n_lambda_t:
        print "add_spectrum_to_template_file: We have a maximum"
        print "number of {0} wavelength points - while you have {1}.".format(n_lambda_t, n_lambda_in)
        print "Some information will be lost."

    new_l = np.zeros(n_lambda_t)
    new_ll = np.zeros(n_lambda_t)
    new_flux = np.zeros(n_lambda_t)
    new_flux_unfilt = np.zeros(n_lambda_t)
    
    new_l[i_start:i_end] = wavelength[i_start:i_end]
    new_ll[i_start:i_end] = np.log10(wavelength[i_start:i_end])
    new_flux[i_start:i_end] = spec_filt[i_start:i_end]
    new_flux_unfilt[i_start:i_end] = flux[i_start:i_end]
    
    # Data to insert.
    if has_desc:
        newspec = [template_number, label, description, redshift, n_lambda_t,
                   new_ll, new_l, new_flux, new_flux_unfilt]

    else:
        newspec = [template_number, label, redshift, n_lambda_t,
                   new_ll, new_l, new_flux, new_flux_unfilt]

    t.add_row(newspec)

    # Add the RMS & Allowed redshift ranges.
    rms_z_range = np.hstack((rms_z_range, np.array(rms_region)[:, np.newaxis]))
    allowed_z_range = np.hstack((allowed_z_range,
                                 np.array(allowed_region)[:, np.newaxis]))

    print "Writing updated table to {0}".format(newfile)
    t.write(newfile, overwrite=True)
    fits.append(newfile, rms_z_range)
    fits.append(newfile, allowed_z_range)
    


def fit_possible_emlines(vacwl, spectrum, spectrum_err, verbose=False, chisq_window=500.0,
                         z_vals=None, plot=False, l_feature=None, ignore_edge=True):
    """
    Fit [O II], C III] and Ly-a lines to a spectrum

    """

    # Take the best-fit redshift and figure out what line that corresponds to.
    # The wavelengths here are vacuum wavelengths, so I use the vacuum
    # wavelength here.
    lines = {'lya': 1215.67, 'ciii': [1906.68, 1908.73],
             'oii': [3727.09, 3729.88], 'ha': 6564.61}
    to_check = ['lya', 'ciii', 'oii']

    l_min = np.min(vacwl)

    z_low_cut_lya = l_min/lines['lya']-1.0
    z_low_cut_ciii = l_min/lines['ciii'][0]-1.0
    z_low_cut_oii = l_min/lines['oii'][0]-1.0

    if plot:
        fig, axes = plt.subplots(1, 3, figsize=(15,6))

    if l_feature is None:
        # Decide on a feature based on the redshifts in z_vals
        if z_vals is None:
            print "AutoZUtilities.fit_possible_emlines: If l_feature is not set, z_vals must be provided!"
            return None

        if z_vals[0] > z_low_cut_lya:
            # We have a Ly-a solution.
            l_feature = lines['lya']*(1+z_vals[0])
            if verbose:
                print "The best-fit solution (z={0:.5f}) would have Ly-a at {1}.".format(z_vals[0], l_feature)

        elif z_vals[0] > z_low_cut_ciii:
            # In this case we will have a C III] solution. In the 
            # original template set this will not occur.
            l_feature = lines['ciii'][0]*(1+z_vals[0])
            if verbose:
                print "The best-fit solution (z={0:.5f}) would have C III] at {1}.".format(z_vals[0], l_feature)
        elif z_vals[0] > z_low_cut_oii:
            # In this case we will have a [O II]
            l_feature = lines['oii'][0]*(1+z_vals[0])
            if verbose:
                print "The best-fit solution (z={0:.5f}) would have [O II] at {1}.".format(z_vals[0], l_feature)
        else:
            l_feature = lines['ha']*(1+z_vals[0])
            if verbose:
                print "The best-fit solution (z={0:.5f}) would have Ha at {1}.".format(z_vals[0], l_feature)
    else:
        if verbose:
            print "fit_possible_emlines: Using l_feature={0}".format(l_feature)

    # We have at this point Ly-a, C III] or [O II]. We now need
    # to refit these and calculate the chi-sq in a narrow window
    # around each line.

    res = dict()
    mn_chisq = 1e30
    mn_line = 'N/A'
    mn_z = -999.
    i_best = -1
    for i, l in enumerate(to_check):
        wl_line = lines[l]
        if type(wl_line) is list:
            z_this = l_feature/(0.5*(wl_line[0]+wl_line[1]))-1.0
            doublet = True
        else:
            z_this = l_feature/wl_line-1
            doublet= False

        restwl = vacwl/(1+z_this)

        # Decide and run a fit.
        if doublet:
            # Here we set a plot window that encompass the two
            # doublets +/- 1000 km/s.
            l_c = 0.5*(wl_line[0]+wl_line[1])
            dw_chisq = wl_line[1]-wl_line[0] + chisq_window*l_c/3e5
            dw = wl_line[1]-wl_line[0] + 2*1000.0*l_c/3e5
            tmp = fit_doublet(restwl, spectrum,
                                  spectrum_err,
                                  wl_line[0], wl_line[1],
                                  dw=dw, use_lmfit=True)
            coeff=tmp[0]
            pcov=tmp[1]
            yfit = tmp[2]

            # Check if the line ratio is up against the edge.
            if np.abs(coeff[1]-0.25) < 0.001 or np.abs(coeff[1]-4.0)<0.001:
                if verbose:
                    print "Coefficient for double up against the edge!"
                edge = True
            else:
                edge = False

            # Calculate the chisq of this fit.
            i_chisq, = np.where(np.abs(restwl-(l_c+coeff[4])) < dw_chisq)
        else:
            # Single Ly-a 
            dw_chisq = chisq_window*wl_line/3e5
            dw = 2*1000.0*wl_line/3e5
            tmp = fit_double_exp(restwl,
                                     spectrum,
                                     spectrum_err, 
                                     wl_line, dw=dw, use_lmfit=True)
            coeff=tmp[0]
            pcov=tmp[1]
            yfit = tmp[2]
            # Calculate the chisq of this fit.
            i_chisq, = np.where(np.abs(restwl-(wl_line+coeff[1])) < dw_chisq)

            # I do not check for edge-cases for the parameters here but maybe I should?
            edge = False

        chisq = np.sum(((spectrum[i_chisq]-yfit[i_chisq])/
                        spectrum_err[i_chisq])**2)/(tmp[3].nfree-1.0)

        # Note that z_this is not a refined redshift!
        res[l] = {'chisq': chisq, 'z_this': z_this, 'coeff': coeff, 'fit': tmp[3]}

        print "I_CHISQ has {0} elements".format(len(i_chisq))
        if plot:
            axes[i].plot(restwl[i_chisq], spectrum[i_chisq])
            axes[i].plot(restwl[i_chisq], yfit[i_chisq], 'r')
            axes[i].set_title("Line {0}".format(l))

        if chisq < mn_chisq:
            if (edge and not ignore_edge) or not edge:
                # We update the chisq if the parameters are not at the edge, 
                # or if ignore_edge is set to False.
                mn_chisq = chisq
                mn_line = l
                mn_z = z_this

        if edge:
            res[l]['chisq'] = -res[l]['chisq']

    # Report on the result
    if verbose:
        print "The best fit is for {0} at z={1} with chisq={2}".format(mn_line, mn_z, mn_chisq)
        print "The full results are: "
        for l in res:
            print "    {0:6s}:  {1}".format(l, res[l]['chisq'])


    # Result array.
    return (mn_line, mn_z, mn_chisq, res)


#################################
# General visualisation routines
#################################
def visualise_cross_correlation(ccinfo, i, xlim=None, ylim=None, indicate=None):
    """
    Look at the cross-correlation function, visualising the RMS z range.
    """

    cc = ccinfo[i]

    fig, ax = plt.subplots(1, 1, figsize=(15,6))
    zs = cc['Redshifts']
    ax.plot(zs, cc['CC'])
    ax.set_xlabel('Redshift')
    ax.set_ylabel('Cross-correlation')

    inside, = np.where((zs >= cc['RMS_Range'][0] ) &
                       (zs <= cc['RMS_Range'][1]))
    ax.plot(zs[inside], cc['CC'][inside], 'r')

    if xlim is not None:
        ax.set_xlim(xlim[0], xlim[1])
    if ylim is not None:
        ax.set_ylim(ylim[0], ylim[1])

    yminmax = ax.get_ylim()
    z1, z2 = cc['Z_Range']
    ax.plot([z1, z1], yminmax, 'purple')
    ax.plot([z2, z2], yminmax, 'purple')
    ax.set_title('Template #{0}'.format(cc['Number'][0]))

    if indicate is not None:
        # Pythonic...
        try:
            for ix in indicate:
                ax.plot([ix, ix], yminmax, 'g')
        except:
            ax.plot([indicate, indicate], yminmax, 'g')
            
    return fig
