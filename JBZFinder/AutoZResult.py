import sys
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
import JBZFinder.AutoZUtilities as azu



#import pdb

class AutoZResult(object):
    """
    Class to capture the results of running AutoZ on a spectrum.

    This class also contains the figure-of-merit calculation. This can be
    altered - and probably should be - for MUSE. 
    
    Attributes:
      wavelength : ndarray
         The wavelength axis of the spectrum as provided as input
      airwl : ndarray
         The wavelengths in air
      vacwl : ndarray
         The wavelengths in vacuum
      spectrum : ndarray
         The spectrum array
      spectrum_err : ndarray
         The uncertainty estimate of the spectrum
      filtered : ndarray
         The filtered spectrum. This is the spectrum after a polynomial
         has been subtracted (stored as `fit`), then a smooth spectrum 
         (stored as `smooth`) has been subtracted and the resulting
         spectrum has been apodized. See section 3.1 in the AutoZ paper. 
      processed_spec_err : ndarray
         The uncertainty on the spectrum after processing. This is the original
         uncertainty array with low values replaced by a median filtered
         version with a filter width of 13 pixels.
      smooth : ndarray
         The smooth component of the spectrum fit. This is combination of a median
         filter with a box-car filter. The parameters of the smoothing used are
         stored in `filter_settings`. See point ii in section 3.1 of the AutoZ 
         paper.
      lowpass : ndarray
         The low-pass part of the spectrum. This is equal to `smooth` + `fit`.
      intermediate : ndarray
         An intermediate stage in the filtering. This is kept for debugging 
         purposes and would normally not be of interest to the user.
      fit : ndarray
         The low-order polynomial fit to the spectrum.

      AAD : float
         The average absolute deviation used to normalise the filtered
         spectrum (calculated by AutoZUtilities.normalise_aad). 
      AAD_indices : ndarray
         The indices of the elements in the spectrum used for the calculation
         of the AAD. Mainly useful for debugging.
      AAD_center : ndarray
         The center around which the AAD is calculated. To preserve consistency
         with the IDL code this is fixed to 0 (see the `center` setting in 
         the call to normalise_aad in :func:`~JBZFinder.AutoZ.process_spectrum`).
      RMS_Norm : float
         The root mean square of the filtered spectrum.
      AA_Norm : float
         The mean of the absolute values of the filtered spectrum

      filter_settings: dict
         A dictionary containing the settings used for the filtering of the 
         spectrum. 
      process_settings: dict
         A dictionary containing the settings used for the processing of the 
         spectrum.
      templatefile : string
         The name of the template file used.
         
      z_peak : list
         An array with the redshifts corresponding to the identified peaks in
         the cross-correlation function. These are ordered according to the 
         height of the normalised cross-correlation function.
      z_fit : list
         The redshifts resulting from a fit of a second-order polynomial to the 
         +/- 3 pixels around the peak. For the default velocity sampling (13.8 km/s),
         this corresponds to +/- 41 km/s. 
      z_origin: list
         Where the redshift comes from. This should be set to trace the origin of 
         the redshifts. In AutoZ this is set to 'CC' to indicate it is a
         cross-correlation redshift. When z_peak is set, this is initialised to 
         an empty list of strings if not already set.
      z_comment: list
         A list of length equal to that of z_peak. When z_peak is set this is 
         initialised to a list of empty strings unless already set.
      peak_template : list
         The number of the best-fit template.
      peak_template_index: list
         The index of the best-fit template in the list that was used for fitting.
         This is needed to index `ccinfo` below.
      FWHM : list
         The full width at half maximum of the peak. This is taken to be 
         the distance between the extremes of the points above half the maximum
         of the cross-correlation peak. This is evaluated within +/- 1000 km/s,
         so it has a maximum value of 2000 km/s. 
      N_FWHM : list
         The number of elements above half-maximum for the cross-correlation peak.
      CCpeak : list
         The peak value of the normalised cross-correlation function corresponding
         to z_peak above.
      CCpeak_raw : list
         The peak value of the non-normalised cross-correlation function.
      x_CCsnippet : list of ndarray
         The x-value (redshift) of a small region of the cross-correlation function
         corresponding to z_peak. The range is +/- 1000 km/s.
      y_CCsnippet : list of ndarray
         The value of the normalised cross-correlation function in a small 
         region around the peak given by z_peak.
      y_CCsnippet_raw : list of ndarray
         The same as y_CCsnippet but for the non-normalised cross-correlation
         function
      y_CCsnippet_fit : list of ndarray
         The fit of a quadratic to the peak of the cross-correlation function, 
         on the same subset index by x_CCsnippet. 
      CC_RMS_Norm : list
         The normalisation applied to the cross-correlation function.
      ccinfo : list of dicts
         A list of the full cross-correlation results for each template. See the 
         documentation for :func:`JBZFinder.AutoZ.do_crosscorr` for details. Note 
         that this is ordered by the number of the templates - to access the right 
         part, please use the `peak_template_index` entry. So ccinfo[peak_template_index[0]]
         gives the information on the cross-correlation results for the best 
         redshift estimate.
      
    """

    
    def __init__(self):
        """
        The AutoZResult class encapsulates the results of running AutoZ.

        The initialiser sets up the various attributes as pointing to None. 
        
        """

        self.wavelength = None
        self.spectrum = None
        self.spectrum_err = None
        self.filtered = None
        self.processed_spec_err = None
        self.smooth = None
        self.lowpass = None
        self.intermediate = None
        self.fit = None

        self.AAD = None
        self.AAD_indices = None
        self.AAD_center = None
        self.RMS_Norm = None
        self.AA_Norm = None

        self.filter_settings = None
        self.process_settings = None
        self.templatefile = None

        self.z_peak = None
        self.z_fit = None
        self.z_origin = None
        self.z_comment = None
        self.peak_template = None
        self.peak_template_index = None
        self.FWHM = None
        self.N_FWHM = None
        self.CCpeak = None
        self.CCpeak_raw = None
        self.x_CCsnippet = None
        self.y_CCsnippet = None
        self.y_CCsnippet_raw = None
        self.y_CCsnippet_fit = None
        self.CC_RMS_Norm = None
        self.ccinfo = None

        
    # I have made the z_peak a property because I want to automagically
    # set z_comment and z_origin if needed.
    @property
    def z_peak(self):
        return self.__z_peak

    @z_peak.setter
    def z_peak(self, x):
        self.__z_peak = x

        if x is not None:
            # Check for the existence of z_comment and z_origin
            # and define these if not set.
            if self.z_comment is None:
                self.z_comment = ['' for i in range(len(self.__z_peak))]
            if self.z_origin is None:
                self.z_origin = ['' for i in range(len(self.__z_peak))]
    
    def calculate_FoM_GAMA(self):
        """
        Calculate the Figure-of-Merit for the redshift peaks using the settings for GAMA.

        Converted from IDL code by Ivan Baldry.
        """

        # The RMS in units of the average absolute deviation. This is 
        # called rms_mad_ratio in the IDL code but as MAD is usually taken to 
        # mean median absolute deviation, I prefer AAD here.
        self.rms_aad_ratio = np.array(self.RMS_Norm)/np.array(self.AA_Norm)

        # Calculate the ratio of the first peak compared to the RMS of peak 2, 3 & 4.
        # See equation 2 in Baldry et al (2014). This requires that the number of peaks
        # is at least 4.
        self.r_x = self.CCpeak[0]
        if self.Npeaks >= 4:
            norm = np.sqrt(sum([v**2 for v in self.CCpeak[1:4]])/3.0)
            self.r_x_ratio = self.r_x/norm
        else:
            print "I cannot calculate FoM with less than 4 peaks"
            return None
        
        # Adjust the FoM - equation 3 of Baldry et al. This might need modifications
        # for other surveys.
        adjusted_rx = 0.4 + 2.8*self.r_x_ratio
        self.cc_fom = self.r_x if self.r_x < adjusted_rx else adjusted_rx
        
        # Adjustment to FoM for large RMS/AAD ratios. Note that this is opposite
        # in sign from what is written in equation 4 in Baldry et al (2014)
        rmr_adjustment = np.clip(1.5*(self.rms_aad_ratio-1.8), 0.0, 2.1)
        self.cc_fom = np.clip((self.cc_fom - rmr_adjustment), 2.6, 1e99)

        # Assign a probability using equation 8 in Baldry et al. The parameters 
        tmp = (self.cc_fom - 3.70)/0.7
        self.prob = (np.tanh(tmp)+1.0)/2.0
        #pdb.set_trace()
        return self.cc_fom, self.prob
        

    def calculate_z_uncertainty_GAMA(self):
        """
        Calculate the redshift uncertainty for each estimate

        Code from convert_z_format.pro in the IDL version of AutoZ - 
        see section 5 in Baldry et al (2014)
        """

        self.velocity_fwhm = np.clip(np.array(self.N_FWHM)*self.velocity_gap, 345., 1104.)
        vel_to_sig = self.velocity_fwhm/(1.0+np.array(self.CCpeak)) # Eq 10.
        self.velocity_err = np.sqrt(19.12**2 + (0.525*vel_to_sig)**2)
        
        return self.velocity_err


    def get_results(self):
        """
        Return the redshift determination results

        Returns:
          z: float
            The most likely redshift estimate
          dz: float
            The uncertainty on the most likely redshift using
            the GAMA calibration. Multiply this with the light
            speed to get the uncertainty in km/s.
          cc_fom: float
            The cross-correlation figure of merit from the GAMA
            algorithms for the best-fit redshift
          prob: float
            The probability of the best-fit redshift being correct
            using the GAMA calibration.
          all: list f dicts
            A list of the different redshift solutions, best to worst. 
        """

        cc_fom, prob = self.calculate_FoM_GAMA()

        dz = self.calculate_z_uncertainty_GAMA()/3e5
        all = []
        for i in range(len(self.z_peak)):
            tmp = {'z': self.z_peak[i],
                   'dz': dz[i],
                   'template': self.peak_template[i]}
            all.append(tmp)

        return self.z_peak[0], dz[0], cc_fom, prob, all

        
    def show_peaks(self, raw=False):
        """
        Show the cross-correlation peaks

        It does the top 4 peaks only.
        """

        fig, axes = plt.subplots(2, 2, figsize=(12,8))

        for i in range(4):
            ix = np.mod(i, 2)
            iy = i/2
            x = self.x_CCsnippet[i]

            if raw:
                y = self.y_CCsnippet_raw[i]
            else:
                y = self.y_CCsnippet[i]
            yfit = self.y_CCsnippet_fit[i]

            axes[ix, iy].plot(x, y)
            axes[ix, iy].set_title('z={0:0.3f} Template={1}'.format(self.z_peak[i], self.peak_template[i]))

            if raw:
                axes[ix, iy].plot(x, yfit, 'r')
            else:
                axes[ix, iy].plot(x, yfit, 'r')
                axes[ix, iy].set_ylim(-1, np.max(yfit)*1.02)
        
        return fig

    def illustrate_fit_and_filter(self):
        """
        Illustrate the effect of fitting of filtering on the spectrum
        
        Inspired by figure 2 in Baldry et al (2014)
        """
        
        
        # Create three subplots above each other that share x axis 
        fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, figsize=(10, 15))
        
        # Show the input spectrum as dots with the best-fit
        # polynomial overlaid.
        ax1.scatter(self.wavelength, self.spectrum, 1, marker=',', color='#aaaaaa')
        ax1.plot(self.wavelength, self.fit, 'r', linewidth=2)
        ok, = np.where(np.isfinite(self.spectrum))
        ylim = np.percentile(self.spectrum[ok], [2.5, 97.5])
        ax1.set_ylim(ylim)
        ax1.text(0.02, 0.92, 'Spectrum with fit', transform=ax1.transAxes,
                 color='blue', backgroundcolor='white', size=16)
            
        # Show the polynomial subtracted spectrum with smooth plot
        # overlain.
        ax2.scatter(self.wavelength, self.spectrum-self.fit, 1, marker=',', color='#aaaaaa')
        ax2.plot(self.wavelength, self.smooth, 'r', linewidth=2)
        ylim = np.percentile(self.spectrum[ok]-self.fit[ok], [2.5, 97.5])
        ax2.set_ylim(ylim)
        ax2.text(0.02, 0.92, 'Subtracted spectrum with smooth', transform=ax2.transAxes,
                 color='blue', backgroundcolor='white', size=16)
    

        ax3.plot(self.wavelength, self.filtered, color='#aaaaaa')
        ax3.text(0.02, 0.92, 'Final filtered spectrum', transform=ax3.transAxes,
                 color='blue', backgroundcolor='white', size=16)
        # Fine-tune figure; make subplots close to each other and hide x ticks for
        # all but bottom plot.
        fig.subplots_adjust(hspace=0)
        plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
        
        return fig


    def show_one_solution(self, i, star=False):
        """
        Illustrate one particular fit.

        """

        z_best = self.z_peak[i]
        t_best = self.peak_template[i]
        
        # Decide, based on the redshift which lines to show. 
        # But if star is set, show absorption line locations instead. 
        if star:
            l_wave = [4861.325, 6562.80, 
                      0.5*(5160.125+5192.625), 5891.94,
                      8498, 8542, 8662]
            l_name = ['Hb', 'Ha', 'Mgb', 'NaD', 'Ca II', 'Ca II', 'Ca II']
        else:
            l_wave = [1215.67, 1907, 0.5*(3726.03+3728.8),
                      4861.325, 5006.843, 6562.80]
            l_name = ['Ly-a', 'C III]1909', '[O II]3727', 'Hb',
                      '[O III]5007', 'Ha']

        
        # The layout I desire is to have one panel spanning the
        # full width. Here I will show the full spectrum + 
        # the best-fit template. The different lines given above
        # are shown on top.
        #
        # Under this I will show a zoom in around a particular line
        # and a snippet of the cross-correlation function.
        #
        ax_spec = plt.subplot2grid((2,3), (0, 0), colspan=3)
        ax_zoom = plt.subplot2grid((2,3), (1, 0))
        ax_cc = plt.subplot2grid((2,3), (1, 1), colspan=2)

        print "Z_best = {0}".format(z_best)
        x = self.wavelength/(1+z_best)
        
        mn_x = np.min(x)
        mx_x = np.max(x)
        yr = [np.nanmin(self.spectrum)*0.98, np.nanmax(self.spectrum)*1.02]
        ax_spec.plot(x, self.spectrum)
        ax_spec.set_xlabel('Wavelength (rest)')
        ax_spec.set_ylabel('Flux')
        ax_spec.set_ylim(yr)
        for l, txt in zip(l_wave, l_name):
            if (l > mn_x) & (l < mx_x):
                ax_spec.plot([l, l], yr, 'r--')
                ax_spec.text(l, yr[1]*1.01, txt)

                
        # Zoom figure
        if z_best < 0.3:
            # Use Ha.
            x_c= 6563.
        elif z_best < 1.45:
            # Use [O II]
            x_c = 3727.
        elif z_best < 2.9:
            # Use C III] - this _WILL FAIL!_
            x_c = 1909.
        else:
            x_c = 1216.
                
        dw = 300
        keep, = np.where(np.abs(x-x_c) < dw)
        if len(keep) > 0:
            x_snip = x[keep]
            y_snip = self.spectrum[keep]
            dy_snip = self.spectrum_err[keep]

            ax_zoom.plot(x_snip, dy_snip, 'gray')
            ax_zoom.plot(x_snip, y_snip, alpha=0.3)
            ax_zoom.set_xlabel('Rest wavelength')
            ax_zoom.plot([x_c, x_c], [0, np.max(y_snip)], 'r--')
        
        # And the cross-correlation function
        ax_cc.plot(self.x_CCsnippet[i], self.y_CCsnippet[i])
        ax_cc.plot(self.x_CCsnippet[i], self.y_CCsnippet_fit[i], 'r')
        ax_cc.set_ylim(-1, np.max(self.y_CCsnippet_fit[i])*1.02)
        
        fig = plt.gcf()
        fig.set_size_inches(15,8)
        
        return fig


    def update_solution_for_aliasing_muse(self, verbose=False, margin=0.05, plot=False):
        """
        Check for aliased solution and use refitting of the spectrum to choose solution

        Note that this only checks for aliasing that can affect the _best_ redshift solution. 
        """

        to_check = self.check_for_aliasing_peaks_muse(verbose=verbose, margin=margin,
                                                      first_only=True)

        if len(to_check) > 0:
            if verbose:
                print "I found {0} aliasing sets and will need to refit".format(len(to_check))

            for d in to_check:
                i1 = d['Index1']
                i2 = d['Index2']
                # This is the wavelength of the putative emission feature.
                l_feature = d['Lambda1']*(1+self.z_peak[i1])
                if l_feature < np.min(self.wavelength) or l_feature > np.max(self.wavelength):
                    # Skip these impossible solution
                    continue
                line, z_best, chisq_best, full = self.fit_possible_emlines(verbose=verbose, plot=plot,
                                                                           l_feature=l_feature)

                # Check whether we want to replace the best solution.
                # I am making a cut at 1000 km/s 
                if 3e5*np.abs(z_best-self.z_peak[0]) > 1000.0:
                    if verbose:
                        print "New best z: {0} Old: ".format(z_best, self.z_peak[0])
                    # Check whether this is the redshift corresponding to i2.
                    if 3e5*np.abs(z_best-self.z_peak[i2]) < 1000.0:
                        # We are ok. So we swap i1 & i2 but update z_origin and z_comment.
                        tmp_z = self.z_peak[i1]
                        tmp_origin = self.z_origin[i1]
                        tmp_comment = self.z_comment[i1]
                        tmp_CCpeak = self.CCpeak[i1]
                        tmp_CCpeak_raw = self.CCpeak_raw[i1]
                        tmp_peakt = self.peak_template[i1]
                        tmp_peaktind = self.peak_template_index[i1]

                        self.z_peak[i1] = self.z_peak[i2]
                        self.z_origin[i1] = self.z_origin[i2]+' + refitting'
                        self.z_comment[i1] = self.z_comment[i2]+' - redshift rank adjusted by refitting'
                        self.z_peak[i2] = tmp_z
                        self.z_origin[i2] = tmp_origin+' + refitting'
                        self.z_comment[i2] = tmp_comment+' - redshift rank adjusted by refitting'
                    else:
                        # This is an unknown redshift
                        if verbose:
                            print "This new redshift was not previously there - inserting a new"

                        self.z_peak.insert(0, z_best)
                        self.z_origin.insert(0, 'New from refitting')
                        self.z_comment.insert(0, 'Found by refitting to line {0}'.format(line))

            
        
    def check_for_aliasing_peaks_muse(self, verbose=False, margin=0.05, first_only=False):
        """
        Test for degenerate [O II], C III] & Ly-a solutions

        The logic here is to look for pairs of redshifts that are possible aliasing fits.
        If the cross-correlation finds the same feature for, say, [O II] and Ly-a, then
        the redshifts should be related through 

        (1+z_Lya)/(1+z_OII) = lambda_OII/lambda_Lya

        and similarly for other pairs of lines. I will consider [O II] & Ly-a,
        [O II] and C III] and C III] and Ly-a. 

        
        
        """

        # Average line wavelengths in vacuum & Angstrom
        l_line = [1215.67, 1907.705, 3727.09] 
        names = ['lya', 'ciii', 'oii']
        N_line = len(l_line)
        line_ratios = []
        ratio_names = []
        ratio_lambda = []
        for i in range(N_line):
            for j in range(i+1, N_line):
                line_ratios.append(l_line[j]/l_line[i])
                ratio_names.append('{0} and {1}'.format(names[j], names[i]))
                ratio_lambda.append([l_line[j], l_line[i]])
        N_peak = len(self.z_peak)
        if first_only:
            # We only want to loop once in this case!
            N_peak_outer = 1
        else:
            # We want all possible pairs.
            N_peak_outer = N_peak
        to_resolve = []
        for i in range(N_peak_outer):
            z1 = self.z_peak[i]
            for j in range(i+1, N_peak):
                z2 = self.z_peak[j]

                # I will create a ratio with the highest redshift on top.
                if z1 < z2:
                    ratio = (1+z2)/(1+z1)
                    swap = False
                else:
                    ratio = (1+z1)/(1+z2)
                    swap = True

                print "Checking {0} vs {1} - r={2}".format(z1, z2,ratio)
                # Loop over lines to check for matches.
                for i_r, r in enumerate(line_ratios):
                    if np.abs(r-ratio) < margin:
                        if verbose:
                            print "Solution {0} (z={1}) and {2} (z={3}) are aliased {4}".format(i, z1, j, z2, ratio_names[i_r])
                        if swap:
                            ind_rs = [1, 0]
                        else:
                            ind_rs = [0, 1]
                        to_resolve.append({'Index1': i, 'Index2': j,
                                           'Lambda1': ratio_lambda[i_r][ind_rs[0]],
                                           'Lambda2': ratio_lambda[i_r][ind_rs[1]]})

        
        return to_resolve


    def fit_possible_emlines(self, **kwargs):
        """
        A convenience wrapper around fit_possible_emlines in AutoZUtilities.
        """
        
        return azu.fit_possible_emlines(self.vacwl, self.spectrum,
                                        self.spectrum_err, z_vals=self.z_peak, **kwargs)
