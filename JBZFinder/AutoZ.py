import sys
import numpy as np
from astropy.io import fits
from astropy.convolution import convolve, Box1DKernel
from scipy.ndimage.filters import maximum_filter
import matplotlib.pyplot as plt
#from JBMath.PolynomialFits import poly_reject

import JBZFinder.AutoZUtilities as azu
import JBZFinder.AutoZResult as azr

#import pdb
    
class AutoZ(object):
    """
    A re-implementation of Ivan Baldry's AutoZ in python. 

    Attributes of general nature:
       debug : boolean
          Set this to true to get some very minimal debugging information.
          This has not been implemented properly yet.

    Attributes related to templates:
       all_templates: recarray
          The full template table, read in from a FITS binary table.
       allowed_z_range: ndarray
          A 2xN_templates array with the allowed redshift ranges
          for each template. If possible this should be read in
          from the FITS file with the templates, but it defaults 
          otherwise to the AutoZ defaults.
       gap10: float
          The step size in the log_lambda_rebin array in log lambda units.
       log_lambda_rebin: ndarray
          The log lambda wavelength array used for the templates. 
       n_all_templates: int
          The number of templates in the template file.
       rms_z_range: ndarray
          A 2xN_templates array with the regions in the spectrum used to 
          calculate the noise in the cross-correlation spectrum.
       templatefile : string
          A string containing the name of the file templates have been 
          read in from.
       templates: list
          A list of those templates that have been requested for
          the processing of a spectrum.
       templates_used: list
          A list of the numbers of the templates used. The number of the
          template is provided in the templatefile as a column TEMPLATE_NUM. 
          This can be extended in the future to named templates.
       velocity_gap: float
          The step size in the log_lambda_rebin array in km/s, calculated
          essentially as gap10*ln(10.0*c, with c being the speed of light.
       
    Attributes related to processing:
       baddata_error_value: float
          The value to set the uncertainty estimate of bad data to.
          The defaults are set by :func:`~JBZFinder.AutoZ.setup_processing`.
       clipvalue: float
          The factor to multiply the mean absolute deviation by before
          clipping, see  :func:`~JBZFinder.AutoZUtilities.normalise_aad` for 
          details and :func:`~JBZFinder.AutoZ.setup_processing` for 
          the default value.
       start_lambda: float
          The start of the wavelength range to consider when fitting the 
          spectrum. The defaults are set by :func:`~JBZFinder.AutoZ.setup_processing`
          and it is quite likely that you want to modify this.
       end_lambda: float
          The end of the wavelength range to consider when fitting the 
          spectrum. The defaults are set by :func:`~JBZFinder.AutoZ.setup_processing`
          and it is quite likely that you want to modify this.
       minval: float
          Only those parts of the spectra lying between minval and
          maxval are used. The defaults are set by :func:`~JBZFinder.AutoZ.setup_processing`
       maxval: float
          Only those parts of the spectra lying between minval and
          maxval are used. The defaults are set by :func:`~JBZFinder.AutoZ.setup_processing`
       offset1: float
          offset1 and offset2 indicate the region in pixel space to
          apodize a singnal towards zero. See :func:`~JBZFinder.AutoZUtilities.apodize_signal`
          for further details.
       offset2: float
          offset1 and offset2 indicate the region in pixel space to
          apodize a singnal towards zero. See :func:`~JBZFinder.AutoZUtilities.apodize_signal`
          for further details.
          
    Attributes related to filtering:
       ndegree: int
          The power of the polynomial fitted to the smooth continuum. The default is 4.
          The default is set in :func:`~JBZFinder.AutoZ.setup_filtering` and is used
          in :func:`~JBZFinder.AutoZ.fit_and_filter`
       median_width: int
          The width of the median window applied to the spectrum in the 
          :func:`~JBZFinder.AutoZ.fit_and_filter` routine. The default is 51 and is
          set in :func:`~JBZFinder.AutoZ.setup_filtering`.
       boxcar_width: int
          The width of the boxcar window applied to the spectrum in the 
          :func:`~JBZFinder.AutoZ.fit_and_filter` routine. The default is 121 and is
          set in :func:`~JBZFinder.AutoZ.setup_filtering`.

    

    Example:
       template_file = TEMP_DIR+'all_temps_v13-11-15-temp33-at-z.fits'

       # Create an AutoZ object
       fz = az.AutoZ(templatefile=template_file)

       # Set processing parameters
       fz.setup_processing(start=4750., end=9300.)

       # Create the output wavelength array.
       fz.create_wavelength_array(verbose=True)

       # And create the new template library.
       to_use = np.concatenate((np.arange(2, 15), np.arange(16, 23), np.array([29, 33])))
       fz.rebin_templates(templates_to_use=to_use)  


    Note:
       The code is based on the IDL version of AutoZ maintained by Ivan Baldry and
       documented in Baldry et al 2014, MNRAS 441:3, 2440-2451
    """


    def __init__(self, templatefile=None):
        """
        Constructor for the AutoZ class

        Attributes:
           templatefile : str (optional)
              The name of a FITS binary file to read the templates from.

        The constructor will read in the templates and set default values
        for the filtering and processing variables through a call to 
        :func:`~JBZFinder.AutoZ.setup_filtering` and :func:`~JBZFinder.AutoZ.setup_processing`.
        """

        self.templatefile = templatefile

        # self.all_templates will contain all templates.
        # This is the raw information from a FITS file.
        self.all_templates = None

        # self.templates contain the templates that are 
        # currently in use and which are interpolated onto
        # a wavelength range.
        self.templates = None
        
        if self.templatefile is not None:
            self.read_templates()

        # Set default processing parameters.
        self.setup_filtering()
        self.setup_processing()

        # Default to debug
        self.debug = 1

    def read_templates(self, filename=None):
        """
        Read in templates for cross-correlation.

        Attributes:
         filename: str (optional)
           The name of the file to read from. If not set, this 
           is taken from self.templatefile, 

        self.templatefile is updated.

        The original format (used in the IDL code) is to read
        one fits binary table with all templates in one array.
        This is not necessarily the optimal solution in the long
        run. A better solution would probably be to have multiple
        extensions and one template per extension.

        This routine also sets self.templates to None. This is to indicate
        that the loaded templatefile has not yet been interpolated onto a
        new wavelength axis.  

        Finally the routine sets the rms_z_range and allowed_z_range. These 
        are crucial variables for the processing of the spectra. There are
        two ways these are specified: if the FITS file has three extensions,
        the rms_z_range is expected to be stored in the second and the 
        allowed_z_range in the third (TODO: The format of the template file
        should be revamped). If this is not the case, it is assumed that the 
        templatefile follows a fixed format copied from the IDL AutoZ code.

        Example: 
           fz = az.AutoZ()
           fz.read_templates(filename='all_temps_v13-11-15-temp33-at-z.fits')
        
        """

        if filename is None:
            filename = self.templatefile

        # The actual reading is done in the load_template_file routine in
        # AutoZUtilities - see that for details of assumptions.
        tdata, rms_z_range, allowed_z_range = azu.load_template_file(filename)
            
        # Setup the template information.
        self.n_all_templates = len(tdata)
        self.all_templates = tdata

        self.rms_z_range = rms_z_range
        self.allowed_z_range = allowed_z_range

        # Finally, set the interpolated template to None.
        self.templates = None
        

    # Refactor processing setup. 
    def setup_processing(self, start=4750, end=9300, minval=-1e4, maxval=1e6, clipvalue=25.0,
                         offset1=10, offset2=60, baddata_error_value=1e10):
        """
        Set up parameters for the process spectrum routine.

        The routine is called when the object is initialised and default values are set,
        it can be called later to alter these.

        Attributes:
           start : float 
             The lowest wavelength to consider for the cross-correlation. Default: 4750. 
           end : float
             The highest wavelength to consider for the cross-correlation. Default: 9300
           minval : float
             Pixels with values below this level are ignored. Default: -1e4
           maxval : float
             Pixels with values above this level are ignored. Default: 1e6
           clipvalue : float
             The value (in units of the average absolute deviation) used to clip
             the spectrum when calculating its average absolute deviation. See
             :func:`~JBZFinder.AutoZUtilities.normalise_aad` for details. Default: 25
           offset1 : int
             The spectrum is apodized between pixel offset1 and offset2 on the left and
             if there are N pixels in the spectrum, between N-offset2 and N-offset1 
             on the right. See :func:`~JBZFinder.AutoZUtilities.apodize_signal` 
             for details. Default: 10
           offset2 : int
             The spectrum is apodized between pixel offset1 and offset2 on the left and
             if there are N pixels in the spectrum, between N-offset2 and N-offset1 
             on the right. See :func:`~JBZFinder.AutoZUtilities.apodize_signal` 
             for details. Default: 60
           baddata_error_value : float
             The value to set the uncertainty estimate of bad data to. Default: 1e10
        """

        self.start_lambda = start
        self.end_lambda = end
        self.minval = minval
        self.maxval = maxval
        self.clipvalue = clipvalue
        self.offset1 = offset1
        self.offset2 = offset2
        self.baddata_error_value = baddata_error_value

        
        
    def create_wavelength_array(self, gap=None, log_scale=True,
                                linear_scale=False, min_value=2.8,
                                max_value=4.0, verbose=False):
        """
        Create a wavelength array to use for cross-correlation
        
        Attributes:
          gap: float
           The gap to use for the wavelength array. Very little sanity
           checking is done but if linear resampling is chosen,
           this has to be >0.01. Default: 2e-5 for log_scale,
           0.2 for linear scale.
    log_scale: boolean
           Set this to create a logarithmically sampled array. Default: True
linear_scale: boolean
           Set this to create a linearly sampled array. Default: False
   min_value: float
           The minimum value to use in the wavelength array. Default: 2.8
   max_value: float
           The approximate maximum value to use in the wavelength array. Default: 4.0

        While max_value is set precisely, it is not given that a particular array
        will reach this because the gap is the main decider here. 
        """

        # I am not using Astropy units for reasons of speed. 
        cspeed = 2.998e5  # km/s
        
        # Set the default gaps
        if log_scale and gap is None:
            gap = 2e-5
        elif linear_scale and gap is None:
            gap = 0.2
            
        if linear_scale and gap < 0.01:
            print "A linear scale and gap < 0.01 is not supported!"
            return None

        if gap <= 0:
            print "The gap must be strictly positive."
            return None

        # I am not using linspace to ensure detailed agreement with the IDL code
        num = np.round((max_value-min_value)/gap)
        x_out = min_value + np.arange(0, num)*gap
        if log_scale:
            velocity_gap = gap*np.log(10.0)*cspeed
        else:
            # It has to be linear scale. In this case the velocity 
            # gap reported is the median across the wavelength range.
            dlx = np.log10(x_out)
            log_gap = dlx[1:]-dlx[:-1]
            velocity_gap = np.median(log_gap)*np.log(10.0)*cspeed

        if verbose:
            print " Wavelength range created:"
            print "     Range: [{0}, {1:.2f}]  Gap: {2} ({3:.2f} km/s) a total of {4} points".format(x_out[0], x_out[-1], gap, velocity_gap, len(x_out))


        #  Set the object property.
        self.log_lambda_rebin = x_out
        self.velocity_gap = velocity_gap
        self.gap10 = (self.log_lambda_rebin[1000]-self.log_lambda_rebin[0])/1000.0
        
        return x_out, velocity_gap

        
    def run_single_spec(self, spec, spec_err, wavelength, in_vacuum=False,
                        Npeaks=4):
        """
        Run AutoZ for a single spectrum.

        Arguments:
          spec : ndarray
             The spectrum array in arbitrary units (but avoid very small or large numbers!)
          spec_err : ndarray
             The uncertainty estimate on the spectrum
          wavelength : ndarray
             The wavelength array associated with the spectrum.

        Attributes:
          in_vaccum : bool
             Set this to True to indicate that the wavelength array is already in 
             vacuum wavelengths. The default is False, in which case the wavelength
             array is converted to vacuum internally. 
          Npeaks : int
             The number peaks in the cross-correlation function to process and return. 
             The default is 4.
           

        Example:
           NRAS 441:3, 2440-2451
    """


        

    # Refactor processing setup. 
    def setup_processing(self, start=4750, end=9300, minval=-1e4, maxval=1e6, clipvalue=25.0,
                         offset1=10, offset2=60, baddata_error_value=1e10):
        """
        Set up parameters for the process spectrum routine.

        The routine is called when the object is initialised and default values are set,
        it can be called later to alter these.

        Attributes:
           start : float 
             The lowest wavelength to consider for the cross-correlation. Default: 4750. 
           end : float
             The highest wavelength to consider for the cross-correlation. Default: 9300
           minval : float
             Pixels with values below this level are ignored. Default: -1e4
           maxval : float
             Pixels with values above this level are ignored. Default: 1e6
           clipvalue : float
             The value (in units of the average absolute deviation) used to clip
             the spectrum when calculating its average absolute deviation. See
             :func:`~JBZFinder.AutoZUtilities.normalise_aad` for details. Default: 25
           offset1 : int
             The spectrum is apodized between pixel offset1 and offset2 on the left and
             if there are N pixels in the spectrum, between N-offset2 and N-offset1 
             on the right. See :func:`~JBZFinder.AutoZUtilities.apodize_signal` 
             for details. Default: 10
           offset2 : int
             The spectrum is apodized between pixel offset1 and offset2 on the left and
             if there are N pixels in the spectrum, between N-offset2 and N-offset1 
             on the right. See :func:`~JBZFinder.AutoZUtilities.apodize_signal` 
             for details. Default: 60
           baddata_error_value : float
             The value to set the uncertainty estimate of bad data to. Default: 1e10
        """

        self.start_lambda = start
        self.end_lambda = end
        self.minval = minval
        self.maxval = maxval
        self.clipvalue = clipvalue
        self.offset1 = offset1
        self.offset2 = offset2
        self.baddata_error_value = baddata_error_value

        
    def rebin_templates(self, log_lambda=None, templates_to_use=None):
        """
        Rebin templates onto log lambda scale and extract the templates to use

        Attributes: 
          log_lambda : ndarray (optional)
             The log lambda array to use as basis. If not set, this will use
             self.log_lambda_rebin which is created by
             :func:`~JBZFinder.AutoZ.create_wavelength_array`
          templates_to_use : list (optional)
             The template numbers to use. If not set, all templates are used.

        Example:

          to_use = np.concatenate((np.arange(2, 15), np.arange(16, 23), np.array([29, 33])))
          fz.rebin_templates(templates_to_use=to_use)

        """

        if log_lambda is None and self.log_lambda_rebin is None:
            print "There is no wavelength array provided and none created"
            print "Please either provide one, or run create_wavelength_array"
            print "to create one."
            return None

        if log_lambda is None:
            log_lambda = self.log_lambda_rebin
        else:
            # Need to ensure consistency between the object attribute
            # and the rebinned template.
            self.log_lambda_rebin = log_lambda

            
        # We start with the templates read in from the FITS file. 
        t = self.all_templates

        # The results are going to be stored in this list
        templates = []

        t_nums = t['TEMPLATE_NUM']
        if templates_to_use is None:
            templates_to_use = t_nums

        # Go through the templates and interpolate them onto
        # the new wavelength axis.
        
        for i_use in templates_to_use:
            # Find which template matches the requested 
            # template number
            i, = np.where(t_nums == i_use)
            if len(i) == 0:
                print "I am unable to locate template number {0}".format(i_use)
                self.templates = None
                return
            
            N = t['NUM_POINTS'][i]
            x_old = t['LOG_LAMBDA'][i, 0:N].flatten()
            y_old = t['SPEC'][i, 0:N].flatten()

            # Ensure that we do not include zero wavelengths in the templates
            # I also sort the array to avoid bugs at the end.
            keep, = np.where(x_old > 0)
            x_old = x_old[keep]
            y_old = y_old[keep]
            si = np.argsort(x_old)
            x_old = x_old[si]
            y_old = y_old[si]
            
            y_new = np.interp(log_lambda, x_old, y_old)

            bad = np.where((log_lambda < x_old[0]) | 
                           (log_lambda > x_old[-1]))
            y_new[bad] = 0.0
            
            d = {'flux': y_new, 'log_lambda': log_lambda,
                 'template_num': t['TEMPLATE_NUM'][i],
                 'rms_z_range': self.rms_z_range[:, i].squeeze(),
                 'allowed_z_range': self.allowed_z_range[:, i].squeeze(),
                 'redshift': t['REDSHIFT'][i]}

            templates.append(d)

        self.templates_used = templates_to_use
        self.templates = templates


    def create_wavelength_array(self, gap=None, log_scale=True,
                                linear_scale=False, min_value=2.8,
                                max_value=4.0, verbose=False):
        """
        Create a wavelength array to use for cross-correlation
        
        Attributes:
          gap: float
           The gap to use for the wavelength array. Very little sanity
           checking is done but if linear resampling is chosen,
           this has to be >0.01. Default: 2e-5 for log_scale,
           0.2 for linear scale.
    log_scale: boolean
           Set this to create a logarithmically sampled array. Default: True
linear_scale: boolean
           Set this to create a linearly sampled array. Default: False
   min_value: float
           The minimum value to use in the wavelength array. Default: 2.8
   max_value: float
           The approximate maximum value to use in the wavelength array. Default: 4.0

        While max_value is set precisely, it is not given that a particular array
        will reach this because the gap is the main decider here. 
        """

        # I am not using Astropy units for reasons of speed. 
        cspeed = 2.998e5  # km/s
        
        # Set the default gaps
        if log_scale and gap is None:
            gap = 2e-5
        elif linear_scale and gap is None:
            gap = 0.2
            
        if linear_scale and gap < 0.01:
            print "A linear scale and gap < 0.01 is not supported!"
            return None

        if gap <= 0:
            print "The gap must be strictly positive."
            return None

        # I am not using linspace to ensure detailed agreement with the IDL code
        num = np.round((max_value-min_value)/gap)
        x_out = min_value + np.arange(0, num)*gap
        if log_scale:
            velocity_gap = gap*np.log(10.0)*cspeed
        else:
            # It has to be linear scale. In this case the velocity 
            # gap reported is the median across the wavelength range.
            dlx = np.log10(x_out)
            log_gap = dlx[1:]-dlx[:-1]
            velocity_gap = np.median(log_gap)*np.log(10.0)*cspeed

        if verbose:
            print " Wavelength range created:"
            print "     Range: [{0}, {1:.2f}]  Gap: {2} ({3:.2f} km/s) a total of {4} points".format(x_out[0], x_out[-1], gap, velocity_gap, len(x_out))


        #  Set the object property.
        self.log_lambda_rebin = x_out
        self.velocity_gap = velocity_gap
        self.gap10 = (self.log_lambda_rebin[1000]-self.log_lambda_rebin[0])/1000.0
        
        return x_out, velocity_gap

        
    def run_single_spec(self, spec, spec_err, wavelength, in_vacuum=False,
                        Npeaks=4):
        """
        Run AutoZ for a single spectrum.

        Arguments:
          spec : ndarray
             The spectrum array in arbitrary units (but avoid very small or large numbers!)
          spec_err : ndarray
             The uncertainty estimate on the spectrum
          wavelength : ndarray
             The wavelength array associated with the spectrum.

        Attributes:
          in_vaccum : bool
             Set this to True to indicate that the wavelength array is already in 
             vacuum wavelengths. The default is False, in which case the wavelength
             array is converted to vacuum internally. 
          Npeaks : int
             The number peaks in the cross-correlation function to process and return. 
             The default is 4.

        Returns:
          The routine returns an AutoZResult object. See that class for a description
          of the basic features.

        Example:
           # Run a basic fit.
           r = fz.run_single_spec(f, df, l, in_vacuum=False)
           # Show the first solution
           r.show_one_solution(0)

        Note:
           This implements the autoz_singlespec.pro routine from the AutoZ distribution.
           The steps it goes through is to filter the spectrum, then to interpolate it 
           onto the log lambda range used by the templates. It is then cross-correlated 
           with each template in turn and find_highest_peaks is called to locate the
           Npeaks highest peaks across the templates.
        """

        # Sanity checking of the wavelength range.
        if (np.min(wavelength) < 1005) or (np.max(wavelength) > 3.2e4):
            print "The run_single_spec routine only supports wavelengths between 0.1 mu<l<3.2 mu."
            return None

        # Sanity warning for the number of peaks.
        if Npeaks < 4:
            print "Warning: The number of peaks must be >= 4 if you want to calculate a\nfigure of merit of the type used by GAMA."
            
        

        specfilt, info = self.process_spectrum(spec, spec_err, wavelength)

        # Create a results object.
        results = azr.AutoZResult()

        # To be more pythonic I should raise an exception in process_spectrum,
        # but I see little gain here so haven't bothered.
        if specfilt is None:
            # Something went wrong in the processing of the spectrum.
            pass
        else:
            # Ok, we are fine. But we need to update the results object. 
            results.wavelength = wavelength
            results.spectrum = spec
            results.spectrum_err = spec_err
            results.velocity_gap = self.velocity_gap
            results.gap10 = self.gap10
            results.templatefile = self.templatefile
            results.filtered = specfilt
            results.processed_spec_err = info['spec_err']
            results.smooth = info['smooth']
            results.lowpass = info['lowpass']
            results.intermediate = info['intermediate']
            results.AAD = info['AAD']
            results.AAD_indices = info['indices']
            results.AAD_center = info['center_used']
            results.RMS_Norm = np.sqrt(np.mean(specfilt**2.0))
            results.AA_Norm = np.mean(np.abs(specfilt))
            results.fit = info['fit']
            results.Npeaks = Npeaks
            
            results.filter_settings = {'ndegree': self.ndegree,
                                       'median_width': self.median_width,
                                       'boxcar_width': self.boxcar_width}
            results.process_settings = {'start_lambda': self.start_lambda,
                                        'end_lambda': self.end_lambda,
                                        'minval': self.minval,
                                        'maxval': self.maxval,
                                        'clipvalue': self.clipvalue,
                                        'offset1': self.offset1,
                                        'offset2': self.offset2,
                                        'baddata_error_value': self.baddata_error_value}
                                        
                                        
  
        # We expect wavelengths in air for input unless in_vacuum is set to True.
        # For use later, the results object will keep a wavelength array in
        # air and vacuum. 
        if in_vacuum:
            log_v_lambda = np.log10(wavelength)
            results.airwl = azu.vactoair(wavelength)
            results.vacwl = wavelength.copy()
        else:
            results.vacwl = azu.airtovac(wavelength)
            log_v_lambda = np.log10(results.vacwl)
            results.airwl = wavelength.copy()
        N_l = len(log_v_lambda)

        # Rebin the spectra on this new scale, ensuring that they go
        # to zero outside the range. We check above that the wavelength
        # range is consistent with this.
        x_tmp = np.concatenate(([3.0, log_v_lambda[0]-0.001], log_v_lambda,
                                [log_v_lambda[-1]+0.001, 4.5]))
        y_tmp = np.concatenate(([0.0, 0.0], specfilt, [0, 0]))
        
        spec_rebin = np.interp(self.log_lambda_rebin, x_tmp, y_tmp)

        # Run cross-correlation
        ccinfo = self.do_crosscorr(spec_rebin, highz=True, helio_vel=0.0)

        # Locate the redshifts through the highest peaks
        p = self.find_highest_peaks(ccinfo, Npeaks=Npeaks)

        #        print [p[i]['RedshiftFit'] for i in range(4)]

        # Insert this information into the results object
        results.z_peak = [p[i]['Redshift'] for i in range(Npeaks)]
        results.z_origin = ['CC-AutoZ' for i in range(Npeaks)]
        results.z_fit = [p[i]['RedshiftFit'] for i in range(Npeaks)]
        results.peak_template = [p[i]['Template'] for i in range(Npeaks)]
        results.peak_template_index = [p[i]['TemplateIndex'] for i in range(Npeaks)]
        results.FWHM = [p[i]['FWHM'] for i in range(Npeaks)]
        results.N_FWHM = [p[i]['N_FWHM'] for i in range(Npeaks)]
        results.CCpeak = [p[i]['CCpeak'] for i in range(Npeaks)]
        results.CCpeak_raw = [p[i]['CCpeak_raw'] for i in range(Npeaks)]
        results.x_CCsnippet = [p[i]['xCCsnippet'] for i in range(Npeaks)]
        results.y_CCsnippet = [p[i]['CCsnippet'] for i in range(Npeaks)]
        results.y_CCsnippet_raw = [p[i]['CCsnippet_raw'] for i in range(Npeaks)]
        results.y_CCsnippet_fit = [p[i]['CCsnippet_fit'] for i in range(Npeaks)]
        results.CC_RMS_Norm = [p[i]['RMS_Norm'] for i in range(Npeaks)]
        
        # I also want to ingest the ccinfo data into the results 
        # object in case we want to inspect this.
        results.ccinfo = ccinfo

        # In the IDL AutoZ code there is at this point a bit of code
        # to fit a quadratic to the peak. I have moved this part of code
        # into the highest peak function.

        
        return results

        
    def do_crosscorr(self, spec_rebin, highz=True, helio_vel=0.0):
        """
        Compute the cross-correlations between spectrum and templates.

        Parameters:
        -----------
        spec_rebin: ndarray
           The object spectrum binned onto the common wavelength
           axis of the templates.

        Keywords:
        ----------
        highz: boolean
          Whether we should consider high-z solutions (this
          expands the search range in redshift). The answer
          should in general be True. Default: True

        helio_vel: float
          Heliocentric velocity of the object if the spectrum
          has not been corrected for heliocentric motion. 
          Default: 0.0


        Returns:
        --------
        res: list of dicts
          This is a list of results for each template. Each element
          is a dictionary with the following entries:

          CC_raw: The unnormalised cross-correlation function.
          CC: The cross-correlation function after normalisation 
              by the RMS of the turning-points in the appropriate 
              part of the redshift range.
          Redshifts: The redshift corresponding to each position in
              cross-correlation function. This can correspond to 
              unphysical redshifts, and in the AutoZ code this is 
              referred to as shifts, but I thought redshifts was 
              clearer for later use.
          Number: The number of the template used.
          PosPeaks: The positive peaks found by the routine. bool array
          NegPeaks: The negative peaks found by the routine. bool array
          TurningPoints: The turning points identified. bool array
          N_TP: The number of turning points.
          RMS_Norm: The normalisation used for the cross-correlation
               function.
          UsefulPeak: A boolean array containing true if the location
               might be a peak. This contains less information than what
               do_crosscorr is returning but it should be sufficient.


        
        
        Converted from the IDL AutoZ code by Ivan Baldry.
        """

        gap = self.gap10
        cspeed = 2.998e5 # km/s
        n_templates = len(self.templates)  # Make into attribute?

        num_corr = 28000 if highz else 14000

        # This temporary array is used in the construction of the
        # redshift map for the cross-correlation function and it is
        # of the same length as the cross-correlation function
        tmp = (np.arange(2*num_corr+1)-num_corr)*gap
        N_cc = len(tmp)

        # The results list.
        results = []
        for i, t in enumerate(self.templates):
            cross_corr_raw = azu.cross_correlate(t['flux'], spec_rebin,
                                                 num_corr)

            # These are the tentative redshifts corresponding to 
            # the correlation function above.
            shifts = 10.0**(tmp)*(1+t['redshift'])*(1+helio_vel/cspeed)-1.0

            # Create some logical arrays.
            allowed_z = ((shifts > t['allowed_z_range'][0])
                         & (shifts <= t['allowed_z_range'][1]))
            use_search, = np.where(allowed_z)
            crit_rms = ((shifts > t['rms_z_range'][0]) & 
                        (shifts <= t['rms_z_range'][1]))
            use_rms, = np.where(crit_rms)

            # Determine where there are local peaks and turning-points.
            pos_peaks = azu.find_peaks(cross_corr_raw)
            neg_peaks = azu.find_peaks(-cross_corr_raw)
            turning_points = pos_peaks | neg_peaks
            n_turning_points = len(np.where(turning_points)[0])
            
            # To symmetrise the negative and positive peaks we subtract
            # off a timmed mean with exclusion of a small fraction of
            # points. The multiplication of the number by 2 is because
            # the AutoZ IDL code for trimmed mean works slightly 
            # different from mine.
            cntr = azu.trimmed_mean(cross_corr_raw[use_rms],
                                           2*len(use_rms)/25)
            cross_corr = (cross_corr_raw - cntr)
                #            print "Center for {0} is {1}".format(i, cntr)
                                  
            # Normalise the correlation function using the 
            # turning points. 
            use_norm, = np.where(crit_rms & turning_points) 
            rms_cross_corr = np.sqrt(np.mean(cross_corr[use_norm]**2))
            cross_corr /= rms_cross_corr

            # Is this a possible useful peak?
            useful_peak = pos_peaks & allowed_z

            #            pdb.set_trace()
            
            # Create the dict of results for output.
            r = {'CC_raw': cross_corr_raw,
                 'CC': cross_corr, 
                 'Redshifts': shifts,
                 'Number': t['template_num'],
                 'PosPeaks': pos_peaks,
                 'NegPeaks': neg_peaks,
                 'TurningPoints': turning_points,
                 'N_TP': n_turning_points,
                 'RMS_Norm': rms_cross_corr,
                 'UsefulPeak': useful_peak,
                 'RMS_Range': t['rms_z_range'],
                 'Z_Range': t['allowed_z_range']}
            
            results.append(r)

        return results


    def find_highest_peaks(self, ccinfo, Npeaks=4):
        """
        Find the highest peaks in a cross-correlation function

        Parameters:
        -----------
        ccinfo:  dict returned from do_crosscorr

        Converted from IDL code created by Ivan Baldry, 
        Reference: Baldry et al. 2014, MNRAS, 441, 2440
        """

        # Get the peaks of the normalised correlation function
        # for each template.
        N_used = len(ccinfo)
        cspeed = 2.99792458e5 # km/s

        template_numbers = []
        index_in_z = []
        redshifts = []
        max_cc = []
        template_index = []
        for counter, cc in enumerate(ccinfo):

            use, = np.where(cc['UsefulPeak'])

            if len(use) > 0:
                # We have at least one useful peak!
                # Push all the results onto the result arrays
                for i in use:
                    template_numbers.append(cc['Number'])
                    template_index.append(counter)
                    redshifts.append(cc['Redshifts'][i])
                    index_in_z.append(i)
                    max_cc.append(cc['CC'][i])
                    

        # Now extract the Npeaks highest peaks. For simplicity I convert the
        # lists to numpy arrays.
        max_cc = np.array(max_cc)
        template_numbers = np.array(template_numbers)
        template_index = np.array(template_index)
        redshifts = np.array(redshifts)
        index_in_z = np.array(index_in_z)
        
        results = []
        for i in range(Npeaks):

            # Find the currently highest peak
            i_max = np.argmax(max_cc)
            z_peak = redshifts[i_max]

            # Get the info for this one.
            cc = ccinfo[template_index[i_max]]
            
            # I also want to store the cross-correlation peak just 
            # around this position, out to +/- 1000 km/s
            z_range = (1+z_peak)*(1 + np.array([-1e3/cspeed, 1e3/cspeed]))-1.0
            x_cc = cc['Redshifts']
            y_cc = cc['CC']
            y_cc_raw = cc['CC_raw']
            keep,  = np.where((x_cc >= z_range[0]) & 
                              (x_cc <= z_range[1]))

            # Fit the peak. In the IDL code this is done
            # using a quadratic over a small range in position. 
            # This is acceptable so I will use that here too.
            nfit = 3 # +/- 3 pixels around the peak
            i_pos = index_in_z[i_max]
            x_fit = cc['Redshifts'][i_pos-nfit:i_pos+nfit+1]
            y_fit = cc['CC'][i_pos-nfit:i_pos+nfit+1]
            f = np.polyfit(x_fit, y_fit, 2)

            # Get the fit
            ply = np.poly1d(f)
            y_cc_fit = ply(x_cc)

            # Do a Gaussian fit too.
            # Commented out for now because curve_fit is not very robust.
            #
            #p0 = [np.max(y_cc[keep]), z_peak, 0.01, np.median(y_cc[keep])]
            #coeff, var_coeff, y_cc_fit_g = azu.gaussfit(x_cc[keep], y_cc[keep],
            #                                           p_initial=p0)

            # Calculate the maximum.
            z_peak_fit = -f[1]/(2*f[0])
            
            # Finally, calculate the width of the peak at half-maximum. This is 
            # evaluated within the +/- 1000 km/s region.
            half_max = 0.5*max_cc[i_max]
            within, = np.where(y_cc[keep] >= half_max)
            n_within = len(within)
            fwhm = np.max(x_cc[within])-np.min(x_cc[within])

            
            # Store this peak in the results.
            tmp = {'Template': template_numbers[i_max],
                   'TemplateIndex': template_index[i_max],
                   'ShiftIndex': index_in_z[i_max],
                   'Redshift': redshifts[i_max],
                   'RedshiftFit': z_peak_fit,
                   'CCpeak': max_cc[i_max],
                   'CCsnippet': y_cc[keep],
                   'CCsnippet_raw': y_cc_raw[keep],
                   'CCsnippet_fit': y_cc_fit[keep],
                   'RMS_Norm': cc['RMS_Norm'],
                   'CCpeak_raw': cc['CC_raw'][index_in_z[i_max]],
                   'xCCsnippet': x_cc[keep],
                   'N_FWHM': n_within,
                   'FWHM': fwhm}
                   
            results.append(tmp)

            # We now exclude this peak and +/- 600 km/s for the next search.
            z_range = (1+z_peak)*(1 + np.array([-600.0/cspeed, 600.0/cspeed]))-1.0
            exclude = np.where((redshifts >= z_range[0]) & 
                               (redshifts <= z_range[1]))
            max_cc[exclude] = -99.
            
        
        return results
        
    def process_spectrum(self, spec, spec_err, wavelength, mimic_idl=False):
        """
        Process one spectrum. 

        Parameters:
        ------------

        spec : ndarray
           The spectrum of the object.
        spec_err : ndarray
           The uncertainty estimate for the object spectrum.
        wavelength : ndarray
           The wavelength array for the spectrum.

        Keywords:
        ----------
        mimic_idl : boolean
           This is a logical switch to ensure that calculations as closely 
           as possible mimic the IDL code. The results do not seem to depend
           strongly on this setting.
        
        Returns:
        --------
        filtered_spec : ndarray
           The spectrum after filtering. This is a multi-step process. The first
           step is a robust high-pass filtering of the spectrum, then the spectrum
           is normalised by the variance estimate in each pixel and bad pixels 
           are set to zero. Finally the filtered spectrum is normalised by the 
           average absolute deviation.

        info : dict
           The details of the calculation are returned in this information dict.

        Notes:
        ------
        Converted from Ivan Baldry's IDL code but not all options are supported.
        The inv_correction option is not included because I feel it is tidier to request 
        that corrections are applied to the spectra before calling this routine.

        """

        # Create two masks indicating whether the spectrum and error are ok or not. 
        criteria_badspec = (np.isfinite(spec) == False) | (spec < self.minval) | (spec > self.maxval)
        criteria_baderror = (np.isfinite(spec_err) == False) | (spec_err <= 0.0)


        # Robust high-pass filtering the spectrum.
        try:
            filtered_spec, info = self.fit_and_filter(wavelength, spec,
                                                      mimic_idl=mimic_idl)
        except ValueError as e:
            print "Fit and filter failed - this spectrum cannot be worked on"
        
            
        if filtered_spec is None:
            return None

        # Create the lowpass spectrum.
        spec_lowpass = info['smooth'] + info['fit']

        # Store this for later.
        spec_intermediate = filtered_spec.copy()

        ok_error = np.where(criteria_baderror == False)

        # Implementation note: Ivan's adjust_broaden.pro is a maximum filter
        # with a kernel of 3 pixels.
        spec_err[ok_error] = maximum_filter(spec_err[ok_error], 3)

        # Apply a minimum uncertainty estimate. 
        # We need to assess whether this is acceptable for MUSE data.
        # 
        # The steps are first to calculate a median filter on the
        # error spectrum, and scale this
        err_med_filt = azu.median_filter_autoz(spec_err[ok_error], 13, mimic_idl=True)*0.7
        # Then replace the error with this median filtered version
        # if the latter is larger.
        tmp = np.vstack((spec_err[ok_error], err_med_filt))
        spec_err[ok_error] = np.max(tmp, 0)

        # Set the uncertainty estimate to a fixed value indicating bad errors where
        # we have bad data.
        is_bad = np.where(criteria_badspec | criteria_baderror)
        spec_err[is_bad] = self.baddata_error_value
        filtered_spec[is_bad] = 0.0 
        
        # Normalise the spectrum relative to the error.
        filtered_spec /= spec_err**2

        # Then clip the spectrum (section 3.1 in AutoZ paper)
        # The center='none' option ensures the same operation as in the IDL code.
        filtered_spec, aad, mean_value, indices = azu.normalise_aad(filtered_spec,
                                                                    clipvalue=self.clipvalue,
                                                                    indices=info['use_fit'], 
                                                               center='none')

        return filtered_spec, {'AAD': aad, 'center_used': mean_value,
                               'indices': indices,
                               'spec_err': spec_err,
                               'filtered': filtered_spec,
                               'smooth': info['smooth'], 'fit': info['fit'],
                               'lowpass': spec_lowpass,
                               'intermediate': spec_intermediate}



    def setup_filtering(self, ndegree=4, median_width=51, boxcar_width=121):
        """
        Setup the default variables for the fit_and_filter routine
        """

        self.ndegree = ndegree
        self.median_width = median_width
        self.boxcar_width = boxcar_width
        
    

    
    def fit_and_filter(self, wavelength, spec, mimic_idl=False):
        """
        Robustly high-pass filter a spectrum and deal properly with filter ends.

        Based on IDL code from Ivan Baldry, see Baldry et al. 2014, MNRAS, 441, 2440
        """
        
        #
        # Extract the fitting region.
        #
        use_fit, = np.where((wavelength > self.start_lambda) & 
                           (wavelength <= self.end_lambda) & 
                           (np.isfinite(spec)))
        is_nan, = np.where(np.isfinite(spec) == False)

        if len(use_fit) <= 0:
            if self.debug:
                print "fit_and_filter: No valid points in spectrum."

            # It might be better to raise an exception here.
            raise ValueError("fit_and_filter: No valid points in spectrum.")

        # AutoZ fits a sigma-clipping polynomial fit to remove a smooth continuum. 
        #  (point i in section 3.1 in the AutoZ paper). 
        
        coeffs, info = azu.poly_reject(wavelength[use_fit], spec[use_fit], self.ndegree, low=3.5,
                                   high=3.5, niterate=15)
        p = np.poly1d(coeffs)
        spec_fit = p(wavelength)

        spec_out = spec - spec_fit
        spec_out[is_nan] = 0.0
                                   

        # The next step is to smooth the spectrum (point ii in section 3.1 in the AutoZ paper)
        spec_smooth = azu.med_smooth(spec_out, self.median_width,
                                     self.boxcar_width, mimic_idl=mimic_idl)
        spec_smooth = convolve(spec_smooth, Box1DKernel(21), boundary='extend')

        # Subtract this smooth spectrum.
        spec_out = spec_out - spec_smooth


        # Taper the end points using a cosine taper.
        spec_out = azu.apodize_signal(wavelength, spec_out, self.start_lambda,
                                  self.end_lambda, self.offset1, self.offset2)

        # Finally set bad regions to zero.
        spec_out[is_nan] = 0.0

        # I will no longer store these as attributes of this object 
        # because they really belong to the result object.
        #        self.spec_filt = spec_out
        #self.spec_fit = spec_fit
        #self.spec_smooth = spec_smooth
        info = {'fit': spec_fit, 'smooth': spec_smooth, 'use_fit': use_fit}
        return spec_out, info




